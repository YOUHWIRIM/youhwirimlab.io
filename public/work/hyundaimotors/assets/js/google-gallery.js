$(function () {

    gallerfy();
    var curPicPos = 0;
    //Pictures details view

    $(".picflex .resultitem > .box > a").click(function (e) {
        //console.log('opn');
        e.preventDefault();
        $('.detailview').slideUp('fast');
        $(".picflex .resultitem").removeClass("open");        
        $(this).parent().parent().addClass("open");
        $(this).parent().next('.detailview').slideDown('fast');
        curPicPos = $(this).parent().attr('data-pos');
        //$(window).scrollTop(curPicPos);
        
    });
    $(".picflex .resultitem").find(".detailNext").click(function () {
        $(this).parent().parent().parent().parent().next().addClass("open").prev().removeClass("open");
        curPicPos = $(this).parent().parent().parent().parent().next().find('.box').attr('data-pos');
        //$(window).scrollTop(curPicPos);
        return false;
    });
    $(".picflex .resultitem").find(".detailPrev").click(function () {
        $(this).parent().parent().parent().parent().prev().addClass("open").next().removeClass("open");
        curPicPos = $(this).parent().parent().parent().parent().prev().find('.box').attr('data-pos');
        //$(window).scrollTop(curPicPos);
        return false;
    });
    $(".picflex .resultitem").find(".detailClose").click(function () {
        $(".picflex .resultitem").removeClass("open");
        $('.detailview').slideUp('fast');
        return false;
    });




});



//$.getScript('//cdn.rawgit.com/xremix/xGallerify/master/dist/jquery.xgallerify.min.js', function () {
function gallerfy() {
    $('.picflex .box').each(function () {
        var boxWidth = $(this).attr('data-imgWidth'),
            boxHeight = $(this).attr('data-imgHeight');
        (boxWidth > boxHeight) ? $(this).parent().attr('style', "width:" + parseInt(boxWidth * 180 / boxHeight) + "px"): $(this).parent().attr('style', "width:" + parseInt(160 / boxHeight * boxWidth) + "px");
        $(this).attr('data-pos', parseInt($(this).offset().top) - 65);
    });
    //var gallery = $('.picflex').gallerify({
    //    mode: {
    //        maxHeight: screen.height * 0.5,
    //        breakPoints: [
    //      {
    //          minWidth: 1170,
    //          columns: 5,
    //      }, {
    //          minWidth: 970,
    //          columns: 4,
    //      }, {
    //          minWidth: 750,
    //          columns: 3,
    //      }, {
    //          maxWidth: 750,
    //          columns: 2,
    //      }
    //        ]
    //    },
    //    lastRow: 'adjust'
    //});
}
//});
