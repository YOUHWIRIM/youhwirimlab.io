// =========================================================
// SVG 포맷 처리 실행 (https://github.com/iconic/SVGInjector)
// =========================================================
/*
(function(global, doc, $) {
  var svgInjection = function() {
    var mySVGsToInject = doc.querySelectorAll('img.inject-svg');
    var injectorOptions = {
        evalScripts: 'once', // always, once, never
        pngFallback: '../images/inject-png', // PNG 대체 폴더 설정
        each: function(svg) {
            //console.log(svg.id);
        }
    };
    SVGInjector(
        mySVGsToInject,
        injectorOptions,
        function(totalSVGsInjected) {
          // console.log(totalSVGsInjected);
        }
    );
  };
  svgInjection();
})(window, document, window.jQuery);*/

// =================================================================
// Scroll Control Plugin
// =================================================================
(function(global, $) {
  // License : https://github.com/cferdinandi/smooth-scroll
  smoothScroll.init();
})(window, window.jQuery);

// License : http://manos.malihu.gr/jquery-custom-content-scroller
(function(global, $) {
  // 스크롤 x 축 적용
  $('[class*="scroll-x"]').each(function(){
    $(this).mCustomScrollbar({
      axis: "x",
      theme:"minimal-dark",
      scrollButtons: {
        enable: true,
        tabindex: 0
      },
      keyboard: {
        enable: true
      },
    });
  });
  // 스크롤 y 축 적용
  $('[class*="scroll-y"]').each(function(){
    $(this).mCustomScrollbar({
      axis: "y",
      theme:"minimal-dark",
      scrollButtons: {
        enable: true,
        tabindex: 0
      },
      keyboard: {
        enable: true
      },
    });
  })
})(window, window.jQuery);

// =================================================================
// Popup Plugin
// =================================================================
// popup - 팝업 버튼, 레이어 팝업 연결
(function(global, $) {
  var $popups = $('.popup').a11y_popup();
  // popup button class
  $('.btn-popup').on('click', function(e) {
    e.preventDefault();
    var popName = $(this).attr('data-id-name');
    var Wsize = $(window).width();
    // popup open layer (data-id)
    $.popupId($popups, popName).open();
  });
})(window, window.jQuery);

// popup - wheels view 360 관련
(function(global, $) {
  if ($('.popup.wheels-view-360-exterior').length > 0) {
    if ($('.popup.wheels-view-360-exterior').parent().css('display') == 'block') {
      if (Wsize >= 861) {
        if (!isVr360Colorchip) {
          isVr360Colorchip = true;
          colorchipSlide();
        }
      }
    }
  }
})(window, window.jQuery);

// popup - wheels video 관련
(function(global, $) {
  if ($('.popup.wheels-video').length > 0) {
    if ($('.popup.wheels-video').parent().css('display') == 'block') {
      
      if (Wsize >= 861) {
        if (!isVideoThumbnail) {
          isVideoThumbnail = true;
          videoThumbnailSlide();
        }
      }
    }
  }
})(window, window.jQuery);

// =================================================================
// SelectBox Option (Email)
// =================================================================
(function(global, $) {
  var selectbox = $("select");
  selectbox.change(function() {
    var $this = $(this),
        select_name = $this.children("option:selected").text();
    $this.siblings("label").children("input").val("")
      .attr("placeholder", select_name)
      .attr("aria-label", select_name);
  });
})(window, window.jQuery);

// =================================================================
// 아코디언 플러그인 실행
// =================================================================
(function(global,$) {
  // 아코디언 설정
  var aco_bases = $(".aco-base");
  var aco_opens = $(".aco-open");
  // 기본 설정 (패널 닫힘)
  $.each(aco_bases,function(index) {
    var aco_base = aco_bases.eq(index);
    new $.Accordion(aco_base);
  });
  // 패널 오픈
  $.each(aco_opens,function(index) {
    //현재 나의 위치 찾기, 모바일 메뉴는 open 되지 않고 호출
    var open_index = 0;
    var aco_open = aco_opens.eq(index);
    if($(this).attr("class") == "aco-open"){
      open_index = $(".aco-open > li").index($(".aco-open > li.on")) + 1;
    }
    new $.Accordion(aco_open, open_index);
  });

})(window, window.jQuery);

// =================================================================
// 탭 메뉴 셀렉트 박스로 실행
// =================================================================
(function(global, $) {
  $.uiTabs('.tab-demo-1, .tab-demo-2');
  var $btn_more_size = $(".tab-select-toggle");
  $btn_more_size.change(function() {
    var $this = $(this),
        select_name = $this.children("option:selected").text();
    $this.siblings("label").children("input").val("")
      .attr("placeholder", select_name)
      .attr("aria-label", select_name);
  });
  $btn_more_size.click(function(e) {
      e.preventDefault();
      var $this = $(this);
      $this.toggleClass("open");
      $this.next().toggleClass("open");
  });
})(window, window.jQuery);

// =================================================================
// 기본 인풋 메세지 제거
// =================================================================
(function(global, $) {
  document.msCapsLockWarningOff = true;
})(window, window.jQuery);


// =================================================================
// 기본 인풋 메세지 제거
// =================================================================
(function(global, doc, $) {
  var sheet = document.createElement('style'),
  $rangeInput = $('.range-type input'),
  prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];
  document.body.appendChild(sheet);
  var getTrackStyle = function (el) {
    var curVal = el.value,
        val = (curVal - 1) * 16.666666667,
        style = '';
    $('.range-type-labels li').removeClass('active selected');
    var curLabel = $('.range-type-labels').find('li:nth-child(' + curVal + ')');
    curLabel.addClass('active selected');
    curLabel.prevAll().addClass('selected');
    // Change background gradient
    for (var i = 0; i < prefs.length; i++) {
      style += '.range-type {background: linear-gradient(to right, #8b949f 0%, #8b949f ' + val + '%, #fff ' + val + '%, #fff 100%)}';
      style += '.range-type input::-' + prefs[i] + '{background: linear-gradient(to right, #8b949f 0%, #8b949f ' + val + '%, #dedfe3 ' + val + '%, #dedfe3 100%)}';
    }
    return style;
  }
  $rangeInput.on('input', function () {
    sheet.textContent = getTrackStyle(this);
  });
  $('.range-type-labels li').on('click', function () {
    var index = $(this).index();
    $rangeInput.val(index + 1).trigger('input');
  });
})(window, document, window.jQuery);