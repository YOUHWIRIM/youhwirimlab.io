var isVr360Colorchip = false;
var colorchipList;
var resize;
var num = 0;

$(function(){
    vrInit();

    function vrInit() {
        var isLoaded = false;
        var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
        var $window = $(window);

        var $target = $('.vr-car-wrap');
        var $targetT = $target;
        var moveType = -1;
        
        var htDefaultOption = {
            moveThreshold: 7,
            slopeThreshold: 50,
        };

        var isStart = false;
        var isMove = false;
        var nVSlope = 0;
        var nHSlope = 0;
        var bSetSlope = false;
        var interval = interval ? 10 : 5;
        
        /*
        1. WAW: 폴라 화이트
        2. U3S: 플래티늄 실버
        3. Z3G: 스파클링 메탈
        4. PAE: 팬텀블랙
        5. Z3G: 아이언 그레이
        6. PR2: 파이어리 레드
        7. R3U: 스타게이징 블루
        8. PF8: 인텐스카퍼
        9. RB4: 데미타스브라운
		10. UB2: 
		11. SG5:
		12. Y3Y:
		13. XFB:
        */

        var colors = ['PR2', 'Z3G', 'R3U', 'PAE', 'U3S', 'WAW', 'PF8', 'RB4', 'UB2', 'SG5', 'Y3Y', 'XFB'];
        
        var vrInfo = {
            model: 'G4S6K4G1UGFCD',
            direction: 0,
            startX: 0,
            startY: 0,
            beforeX: 0,
            beforeY: 0,
            currentColor: colors[0]
        };

        function initMoveInfo() {
            vrInfo.direction = 33;
            vrInfo.color = colors[0];
            vrInfo.startX = 0;
            vrInfo.startY = 0;
            vrInfo.beforeX = 0;
            vrInfo.beforeY = 0;
            $target.find('.sequence').removeClass('on').eq(vrInfo.direction).addClass('on');
        }
        
        // init
        initMoveInfo();
        
        $target.on('touchstart mousedown', function(e){
            if (isMobile) {
                if (e.type == 'mousedown') {
                    return;
                }
            }

            if (!isLoaded) {
                return;
            }

            mRest();

            var htInfo = getMoveInfo(e);

            isStart = true;

            nStartX = htInfo.nX;
            nStartY = htInfo.nY;
            
            idx = vrInfo.direction +1;

            vrInfo.startX = htInfo.nX;
            vrInfo.startY = htInfo.nY;

            //console.log(vrInfo)
        });
        
        if (!isMobile) {
            $targetT = $window;
        }

        $targetT.on('touchmove mousemove', function(e){
            if(!isStart){
                return;
            }

            isMove = true;

            var htInfo = getMoveInfo(e);
            
            vrInfo.beforeX = htInfo.nX;
            vrInfo.beforeY = htInfo.nY;
            
            if (moveType < 0 || moveType == 3 || moveType == 4) {
                moveType = getMoveType(htInfo.nX, htInfo.nY);
            }

            if (moveType == 0) {
                if (vrInfo.beforeX - interval > vrInfo.startX) {
                    idx++;
                    if (idx > 36) idx = 1;
                    vrInfo.startX = vrInfo.beforeX;
                } else if (vrInfo.beforeX + interval < vrInfo.startX) {
                    idx--;
                    if (idx < 1) idx = 36;
                    vrInfo.startX = vrInfo.beforeX;
                }
                vrInfo.direction = idx -1;
                $target.find('.sequence').removeClass('on').eq(vrInfo.direction).addClass('on');
                e.preventDefault();

            } else if (moveType == 1) {
                return;
            }

            if (getMoveType.nDis < htDefaultOption.moveThreshold) {
                return; 
            }
        });
        
        $targetT.on('touchend mouseup', function(e){
            if (isMobile) {
                if (e.type == 'mouseup') {
                    return;
                }
            }
            mRest();
        });

        function mRest(){
            isStart = false;
            isMove = false;
            moveType = -1;
        }

        function setSlope(e) {
            if (!bSetSlope) {
                nHSlope = ((window.innerHeight/2) / window.innerWidth).toFixed(2) * 1;
                nVSlope = (window.innerHeight / (window.innerWidth/2)).toFixed(2) * 1;
                
                return {
                    nHSlope : nHSlope,
                    nVSlope : nVSlope
                }
            }
        }
        
        function getMoveInfo(e) {
            var x = isMobile ? e.originalEvent.touches[0].pageX : e.offsetX;
            var y = isMobile ? e.originalEvent.touches[0].pageY : e.offsetY;
            
            return {
                nX : x,
                nY : y
            }
        }

        function getMoveType(x, y) {
            var nType = moveType;

            var nX = Math.abs(nStartX - x);
            var nY = Math.abs(nStartY - y);
            var nDis = nX + nY;
            
            if (htDefaultOption.slopeThreshold <= nDis) {
                var nSlope = parseFloat((nY/nX).toFixed(2),10);

                if(nSlope <= setSlope().nHSlope){
                    nType = 0;
                }else if(nSlope >= setSlope().nVSlope){
                    nType = 1;
                }else{
                    nType = 2;
                }	
            }
            return nType;
        }
        
        $('.btn-vr360').on('click', vr360);
        $('.btn-prev, .btn-next').on('click', carMove);
        $('#tab1 .colorChips li > div > div').on('click', colorSelect);

        function vr360() {
            imagesProgress(vrInfo.model, vrInfo.direction, vrInfo.currentColor, 'btn-vr360');
            $(this).hide();
        }

        function carMove() {
            if (!isLoaded) {
                return;
            }

            if ($(this).hasClass('btn-prev')) {
                idx = vrInfo.direction +1;
                idx--;
                if (idx < 1) idx = 36;
                vrInfo.direction = idx -1;
            }

            if ($(this).hasClass('btn-next')) {
                idx = vrInfo.direction +1;
                idx++;
                if (idx > 36) idx = 1;
                vrInfo.direction = idx -1;
            }

            $('.vr-car-wrap').find('.sequence').removeClass('on').eq(vrInfo.direction).addClass('on');
        }

        function colorSelect() {
            var idx = $(this).parent().parent().index();
            vrInfo.currentColor = colors[idx];
            imagesProgress(vrInfo.model, vrInfo.direction, colors[idx], 'btn-color');
			//console.log(vrInfo)
        }
        
        function imagesProgress(m, i, c, v) {
            var $target = $('.vr360-wrap');
            var $sequence = $target.find('.sequence');
            var $progressBar = $target.find('.progress > .bar');
            var $progressText = $target.find('.progress > .count');
            
            var idx = i;
            var total = 36;
            var imgSrc = '../../assets/images/buy-online/popup/exterior/vr360/';
        
            initProgress();
        
            if (v == 'btn-vr360') {
                isLoaded = true;
                $target.find('.sequence').empty();
                for (var i=0; i<=total; i++) {
                    t = i;
                    if (t < 10) t = '0' + t;

                    $target.find('.sequence[data-sequence='+ i +']').append('<img src="'+ imgSrc + m +'/'+ t +'/'+ m +'_'+ c +'_'+ t +'.png" alt="">');
                }
            }
        
            if (v == 'btn-color') {
                t = i;
                if (t < 10) t = '0' + t;

                $target.find('.sequence[data-sequence='+ i +']').find('img').attr('src', imgSrc + m +'/'+ t +'/'+ m +'_'+ c +'_'+ t +'.png');

                if (isLoaded) {
                    for (var i=0; i<=total; i++) {
                        t = i;
                        if (t < 10) t = '0' + t;

                        $target.find('.sequence[data-sequence='+ i +']').find('img').attr('src', imgSrc + m +'/'+ t +'/'+ m +'_'+ c +'_'+ t +'.png');
                    }
                }
            }
            
            if (!isLoaded) {
                return;
            }

            var $target = $('.vr360-wrap');
            var $imgLoad = imagesLoaded('.vr360-wrap'),
                imgTotal = $imgLoad.images.length,
                imgLoaded = 0,
                current = 0;
                progressTimer = setInterval(updateProgress, 1000 / 60);
                
                $imgLoad.on('progress', function(){
                    imgLoaded++;
                });
        
            function updateProgress() {
                var target = (imgLoaded / imgTotal) * 100;
                    current += (target - current) * 0.1;

                $progressBar.css({ width: current + '%' });
                $progressText.css('opacity', '1').text( Math.floor(current) +  '%' );
        
                if (current >= 100) {
                    clearInterval(progressTimer);
                    $progressBar.add($progressText).stop().animate({ opacity:0 }, 0, function(){    
                        $('.vr360-wrap').addClass('progress-complete');
                        $('.vr360-wrap').find('.vr-car-wrap').addClass('move');
                    });
                }
            
                if (current >= 99.9) {
                    current = 100;
                }
            }
        
            function initProgress() {
                $target.removeClass('progress-complete');
                $target.find('.vr-car-wrap').removeClass('move');
                $progressBar.css({'width': '0%', 'opacity': '1'});
                $progressText.css('opacity', '0').html('0%');
            }
        }
    }

    $('.popup-content .popup-tab').on('click', 'button', function(){
    var idx = $(this).parent().index();
    var $panel = $(this).closest('.popup-content').find('.content-panel');
    
    $(this).parent().addClass('is-active').siblings().removeClass('is-active');
    $panel.removeClass('is-active').eq(idx).addClass('is-active');

    if (idx == 1) {
      var viewer = pannellum.viewer('panorama', {		
        "default" : {
          "firstScene": "gsy",
          "sceneFadeDuration": 500
        },
        "scenes" : {
          "gsy" : {
            "type": "equirectangular",
            "panorama": "../../assets/images/buy-online/popup/interior/panorama/G4S6K4G1UGFCD/G4S6K4G1UGFCD_GSY.jpg",
            "autoLoad": true
          },
          "try" : {
            "type": "equirectangular",
            "panorama": "../../assets/images/buy-online/popup/interior/panorama/G4S6K4G1UGFCD/G4S6K4G1UGFCD_TRY.jpg",
            "autoLoad": true
          }
        }
      });
      
      $('.interiorchip-list').on('click','button',function(){
        var scene = $(this).attr("data-scene");
        switch(scene) {
          case 'gsy':
            viewer.loadScene('gsy');
            break;
          case 'try':
            viewer.loadScene('try');
            break;
          default:
            viewer.loadScene('gsy'); //first
        }
      });
    }
  });
});

function colorchipSlide() {
  colorchipList = new Swiper('.colorchip-list-inner', {
    slidesPerView: 'auto',
    watchOverflow: true,
    allowTouchMove: false,
    navigation: {
      nextEl: '.colorchip-list-wrap .swiper-button-next',
      prevEl: '.colorchip-list-wrap .swiper-button-prev',
    },
    breakpoints: {
      860: {
        slidesPerView: 2,
        spaceBetween: 10
      }
    }
  });
}

function WsizeChk() {
    var $window = $(window);
    var windowW = $window.width();

    return windowW
}

$(window).on('resize', function(){
  var Wsize = $(window).width();

  if (Wsize >= 861) {
    if (!isVr360Colorchip) {
      colorchipSlide();
    }
  } else if (Wsize < 861) {
    if (isVr360Colorchip) {
      colorchipList.destroy();
    }
  }
  
});