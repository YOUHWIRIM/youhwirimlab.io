﻿$(function(){
	"use strict";
	var swiperPn = [];
	$('.tab-review a').each(function(){
		swiperPn.push($(this).text());
	});
	var swiper = new Swiper('.swiper-container', {
		simulateTouch: false,
		pagination: '.tab-review',
		paginationClickable: true,
		paginationElement:'a',
		paginationBulletRender: function (swiper, index, className) {
			return '<li class="' + className + '"><a>'+swiperPn[index]+'</a></li>';
		}
	});
	$('button[data-target="#selfReview"]').click(function(){
		swiper.update();
		swiper.slideTo(0);
	});
});