﻿$(function(){
	"use strict";
	var swiper = new Swiper('.swiper-container', {
		simulateTouch: false,
		nextButton:'.swiper-button-next',
		prevButton:'.swiper-button-prev',
		slidesPerView:'auto',
        centeredSlides:true,
		loop:false,
		onSlideChangeEnd:function(){
			if($('.swiper-slide-active .tx-a').is(':visible')){
				$('[data-toggle=slider]').parent().addClass('answered');
			} else {
				$('[data-toggle=slider]').parent().removeClass('answered');
			}
		}
	});
	$('[data-toggle=slider]').click(function(){
		$('.swiper-slide-active .tx-a').toggle();
		$(this).parent().toggleClass('answered');
		return false;
	});
});