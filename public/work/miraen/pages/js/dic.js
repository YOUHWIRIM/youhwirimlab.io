var dicList = {
	"awesome": {
		"ko": "멋진, 굉장한",
		"content": "e.g. She is an <i>awesome</i> chef."
	},
	"give away": {
		"ko": "~을 나눠 주다, 기부하다",
		"content": "e.g. She <i>gave</i> a lot of money <i>away</i> to the charity."
	},
	"be afraid of": {
		"ko": "~을 두려워하다",
		"content": "e.g. My little sister <i>is afraid of</i> dogs."
	},
	"cafeteria": {
		"ko": "구내식당",
		"content": "e.g. Is there a <i>cafeteria</i> in this building?"
	},
	"cheerfully": {
		"ko": "기분 좋게, 쾌활하게",
		"content": "e.g. They laughed <i>cheerfully</i> at the joke."
	},
	"coworker": {
		"ko": "동료",
		"content": "e.g. It is important to get along with <i>coworkers</i>."
	},
	"graphic novel": {
		"ko": "만화 소설",
		"content": "e.g. A <i>graphic novel</i> tells a story through its pictures."
	},
	"janitor": {
		"ko": "수위",
		"content": "e.g. He worked as a high school <i>janitor</i>."
	},
	"model": {
		"ko": "(~을) 본떠서 만들다",
		"content": "e.g. The writer <i>modeled</i> the main character on his uncle."
	},
	"nervous": {
		"ko": "긴장한",
		"content": "e.g. Everyone gets <i>nervous</i> on the first day of school."
	},
	"once": {
		"ko": "한때",
		"content": "e.g. My family <i>once</i> lived in Canada."
	},
	"park ranger": {
		"ko": "공원 경비원(관리인)",
		"content": "e.g. I am a <i>park ranger</i>, and I will lead you on the tour."
	},
	"per": {
		"ko": "~당, 매 ~",
		"content": "e.g. Cheetahs can run 30 meters <i>per</i> second."
	},
	"recycle": {
		"ko": "재활용하다",
		"content": "e.g. We should <i>recycle</i> paper, plastic bottles, and cans."
	},
	"scooter": {
		"ko": "스쿠터",
		"content": "e.g. Do you like riding a <i>scooter</i>?"
	},
	"super": {
		"ko": "아주 좋은",
		"content": "e.g. We had a <i>super</i> time in Thailand."
	},
	"unique": {
		"ko": "독특한",
		"content": "e.g. The designer is famous for her <i>unique</i> design."
	},
	"vote": {
		"ko": "투표하다",
		"content": "e.g. Many people will <i>vote</i> for him."
	},
	"while": {
		"ko": "~하는 동안",
		"content": "e.g. I fell asleep <i>while</i> I was studying last night."
	},
	"avoid": {
		"ko": "피하다, 방지하다",
		"content": "e.g. We left early to <i>avoid</i> the rush hour."
	},
	"century": {
		"ko": "100년, 세기",
		"content": "e.g. This city has changed a lot over a <i>century</i>."
	},
	"classical": {
		"ko": "클래식의, 고전의",
		"content": "e.g. I like rap songs better than <i>classical</i> music."
	},
	"cone": {
		"ko": "원뿔",
		"content": "e.g. A <i>cone</i> is a shape that has a pointed top."
	},
	"dust": {
		"ko": "먼지",
		"content": "e.g. He is blowing the <i>dust</i> off the books."
	},
	"enemy": {
		"ko": "적",
		"content": "e.g. Laziness is the <i>enemy</i> of success."
	},
	"essential": {
		"ko": "필수적인",
		"content": "e.g. Wealth is not <i>essential</i> to happiness."
	},
	"goat": {
		"ko": "염소",
		"content": "e.g. A <i>goat</i> is about the size of a sheep."
	},
	"pile up": {
		"ko": "쌓아 올리다",
		"content": "e.g. She <i>piled up</i> the books on the floor."
	},
	"plant": {
		"ko": "식물",
		"content": "e.g. There are many kinds of <i>plants</i> in my grandfather’s garden."
	},
	"round": {
		"ko": "둥근",
		"content": "e.g. The Earth is <i>round</i>."
	},
	"shaped": {
		"ko": "~ 모양의",
		"content": "e.g. I baked some heart-<i>shaped</i> cookies."
	},
	"since": {
		"ko": "~ 이래로",
		"content": "e.g. I have lived in this town <i>since</i> 2005."
	},
	"tax": {
		"ko": "세금",
		"content": "e.g. Avoiding <i>taxes</i> is against the law."
	},
	"thousand": {
		"ko": "천(의)",
		"content": "e.g. The singer has about three <i>thousand</i> fans."
	},
	"view": {
		"ko": "전망",
		"content": "e.g. I’d like a room with an ocean <i>view</i>."
	},
	"without": {
		"ko": "~ 없이",
		"content": "e.g. We cannot live <i>without</i> air and water."
	},
	"wooden": {
		"ko": "나무로된, 목재의",
		"content": "e.g. Mix the soup with a <i>wooden</i> spoon."
	},
	"journal": {
		"ko": "일지",
		"content": "e.g. I keep a <i>journal</i> of daily events."
	},
	"simple": {
		"ko": "단순한",
		"content": "e.g. It is <i>simple</i> to make hot chocolate. "
	},
	"capture": {
		"ko": "포착하다",
		"content": "e.g. I have <i>captured</i> the scene with my camera. "
	},
	"mysterious": {
		"ko": "불가사의한",
		"content": "e.g. Stonehenge is very <i>mysterious</i>. "
	},
	"finally": {
		"ko": "마침내",
		"content": "e.g. After many long days, we <i>finally</i> got to the top of the mountain."
	},
	"remain": {
		"ko": "계속 ~이다",
		"content": "e.g. He <i>remained</i> quiet in the library yesterday. "
	},
	"perfect": {
		"ko": "완벽한",
		"content": "e.g. Nobody is <i>perfect</i>."
	},
	"indoors": {
		"ko": "실내에서, 실내로",
		"content": "e.g. Stay <i>indoors</i> when it rains."
	},
	"B&B (Bed-and-Breakfast)": {
		"ko": "아침 식사를 제공하는 숙소",
		"content": "e.g. It is comfortable to stay at a <i>B&amp;B</i>."
	},
	"admire": {
		"ko": "감탄(존경)하다",
		"content": "e.g. During my trip to Jejudo, I <i>admired</i> the amazing view."
	},
	"plate": {
		"ko": "접시",
		"content": "e.g. She is putting food on her <i>plate</i>."
	},
	"object": {
		"ko": "물건",
		"content": "e.g. Be careful when you lift heavy <i>objects</i>."
	},
	"hall": {
		"ko": "홀",
		"content": "e.g. The <i>hall</i> was crowded with people last night."
	},
	"portrait": {
		"ko": "초상화",
		"content": "e.g. The painter has left many <i>portraits</i> of his lover."
	},
	"graduate": {
		"ko": "졸업하다",
		"content": "e.g. She has <i>graduated</i> from a famous university."
	},
	"university": {
		"ko": "대학",
		"content": "e.g. Harvard is one of the most famous <i>universities</i> in the U.S."
	},
	"avatar": {
		"ko": "아바타",
		"content": "e.g. Make your own <i>avatar</i> and enjoy the game!"
	},
	// 4단원 read 페이지
	"generation": {
		"ko": "세대, 시대",
		"content": "e.g. Culture changes through <i>generations</i>."
	},
	"trusty": {
		"ko": "믿음직한",
		"content": "e.g. Jaden is my <i>trusty</i> friend."
	},
	"respect": {
		"ko": "존경하다",
		"content": "e.g. I <i>respect</i> my mother the most."
	},
		"lost": {
		"ko": "(길을) 잃은",
		"content": "e.g. I easily get <i>lost</i> when I go to strange places."
	},
	"disease": {
		"ko": "질병, 병",
		"content": "e.g. Mrs. Smith is suffering from Alzheimer's <i>disease</i>."
	},
	"wander off": {
		"ko": "돌아다니다",
		"content": "e.g. Don't <i>wander off</i> at night."
	},
	"keep an eye on": {
		"ko": "~을 계속 지켜보다",
		"content": "e.g. Will you <i>keep an eye on</i> my boy while I’m away?"
	},
	"pressure": {
		"ko": "압력",
		"content": "e.g. Air<i> pressure</i> decreases when you go up the mountain."
	},
	"sensor": {
		"ko": "감지 장치, 센서",
		"content": "e.g. The <i>sensor</i> is not receiving any signal now."
	},
	"heel": {
		"ko": "발뒤꿈치",
		"content": "e.g. There is a hole in the <i>heel</i> of my sock."
	},
	"signal": {
		"ko": "신호",
		"content": "e.g. Pay attention to traffic <i>signals</i> when you are driving."
	},
	"material": {
		"ko": "재료, 소재",
		"content": "e.g. Wood is the most important <i>material</i> for making paper"
	},
	"comfortable": {
		"ko": "편안한, 쾌적한",
		"content": "e.g. This bed is pretty <i>comfortable</i>."
	},
	"trial": {
		"ko": "시행, 시도, 재판",
		"content": "e.g. Thomas Edison invented the light bulb after much <i>trial</i> and error"
	},
	"succeed": {
		"ko": "성공하다",
		"content": "e.g. The soccer player finally <i>succeeded</i> in scoring a goal."
	},
	"device": {
		"ko": "장치, 기구",
		"content": "e.g. He made a safety <i>device</i> for his baby daughter."
	},
	"perform": {
		"ko": "공연하다, 수행하다",
		"content": "e.g. Actors <i>perform</i> the play on stage."
	},
	"storyline": {
		"ko": "(소설·연극·영화 등의) 줄거리",
		"content": "e.g. The <i>storyline</i> of the movie is very interesting."
	},
	"appreciate": {
		"ko": "감상하다, 감사하다, 알아주다",
		"content": "e.g. Background knowledge will help you to <i>appreciate</i> the musical."
	},
	"riddle": {
		"ko": "수수께끼",
		"content": "e.g. Nobody could answer this <i>riddle</i> for many years."
	},
	"be able to": {
		"ko": "~할 수 있다",
		"content": "e.g. He <i>isn’t able to</i> walk because he has broken his leg."
	},
	"dawn": {
		"ko": "새벽, 동틀 녘",
		"content": "e.g. It’s <i>dawn</i>. The sun is rising."
	},
	"aria": {
		"ko": "아리아(오페라의 클라이맥스에서 주인공이 단독으로 부르는 곡)",
		"content": "e.g. “The Queen of the Night” is a famous <i>aria</i> from the opera <i>The Magic Flute</i>."
	},
	"mean": {
		"ko": "~을 뜻하다",
		"content": "e.g. What does it <i>mean</i>? I can’t understand."
	},
	// 6단원 read
	"half": {
		"ko": "반(의), 절반(의)",
		"content": "e.g. He is <i>half</i> man and half beast."
	},
	"dive into": {
		"ko": "~으로 뛰어들다",
		"content": "e.g. The students <i>dove into</i> the science experiment."
	},
	"official": {
		"ko": "신하, 관리, 공무원",
		"content": "e.g. She is an <i>official</i> in the government."
	},
	"offer": {
		"ko": "제공하다, 제안하다",
		"content": "e.g. He has <i>offered</i> me some advice."
	},
	"ingredient": {
		"ko": "재료, 양념",
		"content": "e.g. I have added a special <i>ingredient</i> to the food."
	},
	"symbolize": {
		"ko": "상징하다",
		"content": "e.g. The color red <i>symbolizes</i> luck in Chinese culture."
	},
	"political": {
		"ko": "정치의, 정당의",
		"content": "e.g. The U.S. has two main <i>political</i> parties."
	},
	"party": {
		"ko": "정당",
		"content": "e.g. The <i>party</i> needs to solve the problem."
	},
	"policy": {
		"ko": "정책",
		"content": "e.g. The government has announced a new <i>policy</i>."
	},
	"scholar": {
		"ko": "학자",
		"content": "e.g. Yulgok was a great <i>scholar</i> of Joseon."
	},
	"pot": {
		"ko": "냄비, 솥",
		"content": "e.g. Add some milk and chicken to the <i>pot</i>."
	},
	"imaginary": {
		"ko": "상상의",
		"content": "e.g. The story is based on an <i>imaginary</i> place."
	},
	"fortune teller": {
		"ko": "점술가, 역술인, 점쟁이",
		"content": "e.g. The king needs a <i>fortune teller</i> to know the future of his kingdom"
	},
	// 7단원 read 페이지
	"lamp": {
		"ko": "램프, 등 ",
		"content": "e.g. Turn on the <i>lamp</i> when it gets dark."
	},
	"electricity": {
		"ko": "전기",
		"content": "e.g. The  <i>electricity</i>  is off because of the storm. "
	},
	"daytime": {
		"ko": "낮",
		"content": "e.g. The <i>daytime</i> gets longer in the summer."
	},
	"ceiling": {
		"ko": "천장",
		"content": "e.g. Can you catch the mosquito on the <i>ceiling</i>?"
	},
	"blackout": {
		"ko": "정전",
		"content": "e.g. Strong lightning can cause a <i>blackout</i>."
	},
	"bleach": {
		"ko": "표백제",
		"content": "e.g. Use <i>bleach</i> to make your clothes clean."
	},
	"hole": {
		"ko": "구멍",
		"content": "e.g. I am digging a <i>hole</i> to plant a tree."
	},
	"bend": {
		"ko": "구부러지다, 휘다 ",
		"content": "e.g. <i>Bend</i> the straw to use it easily."
	},
	"spread": {
		"ko": "퍼뜨리다, 펼치다 ",
		"content": "e.g. Don’t <i>spread</i> rumors about other people."
	},
	"charity": {
		"ko": "자선단체",
		"content": "e.g. The <i>charity</i> helps poor children in our country. "
	},
	"install": {
		"ko": "설치하다",
		"content": " e.g. It is easy to <i>install</i> Moser lamps. "
	},
	//8단원 read 페이지
	"race": {
		"ko": "경주 ",
		"content": "e.g. The Iditarod <i>Race</i> takes place in Alaska. "
	},
	"trail": {
		"ko": "코스, 흔적 ",
		"content": "e.g. This <i>trail</i> will lead us to the top of this mountain. "
	},
	"snowstorm": {
		"ko": "눈보라",
		"content": "e.g. The flight has been delayed because of the <i>snowstorm</i>."
	},
	"terrible": {
		"ko": "끔찍한 ",
		"content": "e.g. I had a <i>terrible</i> dream last night. "
	},
	"medicine": {
		"ko": "약 ",
		"content": "e.g. Take this <i>medicine</i>, and you will get better soon. "
	},
	"relay": {
		"ko": "릴레이(이어달리기)",
		"content": "e.g. Our class won the <i>relay</i> race on the sports day."
	},
	"crack": {
		"ko": "갈라지다, 금이 가다",
		"content": "e.g. The old wall has started to <i>crack</i>."
	},
	"reach": {
		"ko": "~에 이르다 ",
		"content": "e.g. Go straight until you <i>reach</i> the main street."
	},
	"continue": {
		"ko": "계속 가다",
		"content": "e.g. A terrible snowstorm hit us, but we <i>continued</i> on our way."
	}
};
