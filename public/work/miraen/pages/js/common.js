﻿$(function(){
	"use strict";
	// layer
	$('[data-toggle=layer]').click(function(){
		$('[data-toggle=layer]').not($(this)).removeClass('on');
		$(this).toggleClass('on');
		var o = $(this).attr('data-target');
		audioStop();
		$('.layer').not(o).hide().parents('.layer-wrp').removeClass('open');
		$(o).toggle().parents('.layer-wrp').toggleClass('open');
	});
	$('body').on('click','[data-toggle=layerClse]',function(event){
		if($(event.target).is('a,button,[role=button]') || $(event.target).closest('a,button,[role=button]').length){
			return;
		}
		$('[data-toggle=layer]').removeClass('on');
		$('.layer').hide().parents('.layer-wrp').removeClass('open');
		if($(this).attr('data-type')==='final'){
			$(this).closest('assessmentItem').find('[data-toggle=answer-final]').click();
		}
	});
	$('[data-toggle=btn-think]').click(function(){
		$(this).next().toggle();
		audioStop();
	});
	$('button.btn-selfchk').click(function(){
		$(this).parents('.self-chk').toggleClass('open');
	});
	$('body').on('click','[data-toggle=tab]',function(){
		$(this).parent().addClass('active').siblings().removeClass('active');
		$($(this).attr('href')).addClass('active').siblings().removeClass('active');
		return false;
	})
	.on('click','[data-toggle=tab-review]',function(){
		$(this).parent().toggleClass('active').siblings().removeClass('active');
		$($(this).attr('href')).toggleClass('active').siblings().removeClass('active');
		return false;
	});

	// example
	$('[data-toggle=example]').click(function(){
		$($(this).attr('data-target')).children().toggleClass('active');
	});
	$('[data-toggle=example-multi]').click(function(){
		var o = $($(this).attr('data-target')),
            o2 = o.children('.active');
        if(o2.next().length){
		  o2.removeClass('active').next().addClass('active');
        } else {
		  o2.removeClass('active');
          o.children(':first-child').addClass('active');
        }
	});

	// modal
	$('[data-toggle=modal]').click(function(){
		$($(this).attr('data-target')).addClass('in');
		audioStop();
		return false;
	});
	$('[data-dismiss=modal]').click(function(){
		$('.modal').removeClass('in');
		audioStop();
		return false;
	});
	$('button.btn-narr').click(function(){
		$(this).parents('.modal').find('.narr').slideToggle(250);
	});

	// answer
	$('[data-toggle=answer]').click(function(){
		var o = $($(this).attr('data-target'));
		$(this).toggleClass('on');
		o.find('.tx-a').toggle();
		o.find('.red-circle,.answer-lenis,.arrow-top-red,.arrow-bot-red').toggleClass('active');
		if(!$(this).parent().hasClass('script-ctr') && !$('.video-area.sm').length){
			audioStop();  
		}
	});
	$('[data-toggle=refresh]').click(function(){
		$($(this).attr('data-target')).find('.answer-wrp .tx-a').hide();
		return false;
	});
	// final answer
	// 데이터 전송 준비
	$('assessmentItem').each(function(){
		var t = $(this);
		var id = t.find('input:first,textarea:first').attr('id');
		t.attr('data-qid',id);
	});
    // 선택항목에 값 넣기 (*개발참조 : 넣을 필요없이 직접 html에서 정답을 지정해주면 되었는데 value혼동으로 인해 그냥 이런 불필요한 방식으로 유지함.)
	var namePrev='';
	$('input[type=radio],input[type=checkbox]').each(function(){
		var t = $(this);
		var name = t.attr('name');
		if(namePrev===name){
			return;
		}
		$('input[type=radio],input[type=checkbox]').filter('[name='+name+']').each(function(i){
			if(!t.val()){
				$(this).val(i+1);
			}
		});
		namePrev = name;
	});
	$('[data-toggle=answer-all]').click(function(){
		var t = $(this);
		var o = $(t.attr('data-target'));
		o.find('.red-line').toggleClass('active');
		t.toggleClass('on');
		if(t.hasClass('a-checked')){
			t.removeClass('a-checked');
			o.find('[data-toggle=answer-final]').filter('.a-checked').click();
		} else {
			t.addClass('a-checked');
			o.find('[data-toggle=answer-final]').not('.a-checked').click();
		}
		o.find('[data-toggle=answer]').click();
		return false;
	});
	$('[data-toggle=answer-re]').click(function(){
		var o = $($(this).attr('data-target'));
		if(!$(this).attr('data-target')){
			o = $('#wrap');
		}
		o.find('[data-toggle=answer-final].a-checked,.q-line .icon-re').click();
		o.find('input,textarea').val('').prop('checked','');
	});
	$('[data-toggle=answer-final]').click(function(){
		var o = $($(this).attr('data-target'));
		$(this).toggleClass('a-checked');
		o.toggleClass('answered').find('.tx-a').toggle();
		if(!o.hasClass('answered')){
			o.removeClass('answer-o answer-x').find('.answer-chk').removeClass('answer-chk');
			o.find('.layer').hide();
			// 사용자 입력데이터 삭제
			var delItm;
			if(parent.API_ANNOTATION_INPUT_DELETE){
				parent.API_ANNOTATION_INPUT_DELETE(delItm);
			}
			return;
		}
		var isCorrect = null,
			answer = '',
            aV = o.find('input[data-answer]');
		if(o.attr('data-qtype')==='radio'){
			// radio
			aV.addClass('answer-chk');
			var num = aV.length;
			if(o.find('input[data-answer]:checked').length===num){
				o.addClass('answer-o');
				isCorrect = true;
			} else {
				o.addClass('answer-x');
				isCorrect = false;
			}
			answer = aV.val();
		} else if(o.attr('data-qtype')==='checkbox'){
			// checkbox
			aV.addClass('answer-chk');
			var c = aV.length;
			if(o.find('input[data-answer]:checked').length===c && o.find('input:checked').length===c){
				o.addClass('answer-o');
				isCorrect = true;
			} else {
				o.addClass('answer-x');
				isCorrect = false;
			}
			aV.each(function(i){
                if(i===0){
                    answer = $(this).val();
                } else {
                    answer += ','+$(this).val();
                }
			});
        } else if(o.find('assessmentItem').attr('data-response-type')==='essay'){
			// essay
			isCorrect = true;
			o.find('.layer').show();
			answer = o.find('.layer').text().replace(/\s|/gi, '');
        } else if(o.attr('data-qtype')==='multi'){
			// multi
            if(o.find('input[type=radio]').length){
                // radio
                aV.addClass('answer-chk');
                var num = aV.length;
                if(o.find('input[data-answer]:checked').length===num){
                    o.addClass('answer-o');
                    isCorrect = true;
                } else {
                    o.addClass('answer-x');
                    isCorrect = false;
                }
                if(o.find('.tnf-wrp').length){
                    // TrueFalse
                    o.find('input[data-answer]').each(function(i){
						if(i===0){
							answer = $(this).val();
						} else {
							answer += ','+$(this).val();
						}
                    });
                } else {
                    o.find('input[data-answer]').each(function(i){
                        if(i===0){
                            answer = $(this).next().text();
                        } else {
                            answer += ','+$(this).next().text();
                        }
                    });
                }
            } else {
                o.find('input[type=text]').each(function(){
                    var tA = $(this).attr('data-answer');
                    if(tA===undefined){
                       tA = '';
                    }
                    if($(this).val().toLowerCase()!==tA.toLowerCase()){
                        o.addClass('answer-x');
                        isCorrect = false;
                        return false;
                    }
                    o.addClass('answer-o');
                    isCorrect = true;
                });
                o.find('input[type=text],textarea').each(function(i){
                    if(i===0){
                        answer = $(this).attr('data-answer');
                    } else {
                        answer += ','+$(this).attr('data-answer');
                    }
                });
            }
		} else {
			// text
            var t = o.find('input[type=text]:not(:disabled),textarea');
			var v = t.val();
			if(v===o.attr('data-answer')){
				o.addClass('answer-o');
				isCorrect = true;
			} else {
				o.addClass('answer-x');
				isCorrect = false;
			}
			answer = t.attr('data-answer');
		}
		// 데이터 전송
		var $itemObject = $(this).closest('assessmentItem'),
			userVal=[],
			userDesc=$('h1:first>strong').text();
		if(!$itemObject.length){
			$itemObject = $(this).nextAll('assessmentItem');
		}
		if($('#selfReview').hasClass('in')){
			userDesc=$('#selfReview h2').text();
		}
		o.find('input:not([type=range]),textarea').each(function(i){
			var t = $(this);
			if(t.is('[type=text]') || t.is('textarea')){
				userVal.push(t.val());
			} else if(t.is(':checked')){
				userVal.push(t.val());
			}
		});
		if(o.hasClass('no-answered')){
			isCorrect = true;
		}
		DTCaliperSensor.fire({
			correct: isCorrect,
			itemObject:$itemObject[0],
			value:o.find('correctResponse').text().replace(/[\n\t\r]/g,''),
			userValue:userVal.join(','),
			description:userDesc+' '+$itemObject.find('.progress-q>b,.tit2>b,h3>b').text(),
			pageNumber:$('#num b').text()
		});
		return false;
	});

	// accent
	$('[data-toggle=accent]').click(function(){
		var o = $(this).attr('data-target');
		$(this).parent().toggleClass('answered');
		$(o).toggleClass('accent-show');
	});

	// starrating
	$('.starrating label').click(function(){
		$(this).addClass('active').prevAll().addClass('active').end().nextAll().removeClass('active');
	});

	// self test
	$('button.btn-self-test').click(function(){
		$(this).next().slideToggle();
		return false;
	});

	// dic
	$('.btn-dic').each(function(){
		var t = $(this),
			tTx = t.text(),
			tL = t.offset().left,
			tK = dicList[tTx].ko,
			tC = dicList[tTx].content,
			ll = '';
		if(tC.length>60){
			ll = ' layer-limit';
		}
		t.wrap('<span class="layer-wrp explanation-wrp '+t.attr('data-pos')+'"></span>').append('<dl class="explanation'+ll+'" data-toggle="layer-dic" style="width:'+t.attr('data-width')+'px"><dt><b>'+tTx.replace('&',"&amp;")+'</b> '+tK+'</dt><dd>'+tC+'</dd></dl>');
	})
	.click(function(){
		$('.explanation').hide();
		$(this).children('.explanation').toggle().end().parent().addClass('open').siblings().removeClass('open').find('.layer').hide();
	});
	$('body').on('click','[data-toggle=layer-dic]',function(){
		$(this).hide();
	});

	//script
	$('[data-toggle=script-stop]').click(function(){
		audioStop();
	});

	// audio
	$('body').append('<audio id="audioPlayer"></audio>');
	var audioPlayer = $('#audioPlayer');
	$('body').on('click','[data-toggle=audio]',function(){
		var t = $(this);
		var o = audioPlayer[0];
		o.muted = false;
		$('.muted').removeClass('muted');
		$('.audio-player input.volumebar').val(1);
		// ie대응
		t.addClass('playing');
		if(audioPlayer.attr('src')!==$(t.attr('data-target')).attr('data-src')){
			audioPlayer.attr('src',$(t.attr('data-target')).attr('data-src'))
			.off()
			.on('play',function(){
				//t.addClass('playing');
			})
			.on('ended',function(){
				t.removeClass('playing');
				o.pause();
				o.currentTime = 0;
			});
			$('.audio-wrp').removeClass('open');
			$('.playing,.playing-script').not(this).removeClass('playing playing-script');
		}
		// type
		if(t.attr('data-type')==='script'){
			if(t.closest('.modal-script').find('.script-cnt .playing').length){
				audioStop();
			}
			if(o.paused){
				o.play();
				t.addClass('playing');
			} else {
				o.pause();
				t.removeClass('playing');
			}
		} else if(t.hasClass('icon-speaker')){
			$('.playing').removeClass('playing');
			if(o.paused){
				o.play();
				t.addClass('playing');
			} else {
				o.pause();
				t.removeClass('playing');
			}
		} else if(t.attr('data-type')==='q'){
			o.volume = 1;
			$('.audio-player input.volumebar').val(1);
			$('.playing').removeClass('playing');
			$('.audio-wrp').removeClass('open');
			if(!t.parent().hasClass('open')){
				t.parent().addClass('open');
			}
			if(o.paused){
				o.play();
				t.addClass('playing');
			} else {
				o.pause();
				t.removeClass('playing');
			}
			audioPlayer.on('loadedmetadata',function(){
				var playSec = parseInt($(this)[0].duration%60);
				if(playSec<10){
					playSec = '0'+playSec;
				}
				$('.audio-player div.status').text('0'+parseInt($(this)[0].duration/60)+':'+playSec);
			})
			.on('timeupdate',function(){
				var t = $(this)[0];
				$('.audio-player input.playbar').val(parseInt(t.currentTime/t.duration*100));
			});
		} else if(t.attr('data-type')==='word111'){
			
		} else {
			o.play();
		}
		// caption
		if(t.attr('data-caption')){
			audioPlayer.on('timeupdate',function(){
				var time = this.currentTime;
				$(t.attr('data-caption')).find('[data-toggle=audio]').each(function(){
					var pt = String($(this).attr('data-playtime'));
					var playtime = pt.split(',')[0];
					var endtime = pt.split(',')[1];
					if(playtime<time && time<endtime){
						$(this).addClass('playing-script');
					} else {
						$(this).removeClass('playing-script');
					}
				});
			});
		}
	});
	// audio q
	$('[data-toggle=audio][data-type=q]').each(function(){
		$(this).wrap('<div class="audio-wrp"></div>').after('<div class="audio-player"><div><input type="range" class="playbar" min="0" max="100" step="1" value="0" /></div><div class="status">00:00</div><div><button type="button" class="icon-mute">mute</button></div><div><input type="range" class="volumebar" min="0" max="1" step=".1" value="1" /></div><button type="button" class="icon-audio-clse" onclick="audioStop();$(this).parent().removeClass(\'open\')">닫기</button></div>');
	});
	$('.audio-player input.volumebar').on('input change',function(){
		audioPlayer[0].volume = this.value;
	});
	$('.audio-player button.icon-mute').click(function(){
		var t = $(this),
			o = audioPlayer[0];
		if(o.muted){
			t.removeClass('muted');
			$('.audio-player input.volumebar').val(1);
			o.volume = 1;
			o.muted = false;
		} else {
			t.addClass('muted');
			$('.audio-player input.volumebar').val(0);
			o.muted = true;
		}
	});
	$('.audio-player input.playbar').on('mousedown touchstart',function(){
		audioPlayer[0].pause();
	})
	.on('mouseup touchend',function(){
		var o = audioPlayer[0];
		o.currentTime = parseInt(o.duration/100*this.value);
		o.play();
	});
	$('[data-toggle=audio-fake]').click(function(){
		var t = $(this);
		if(t.find('.answer-chk').length || t.find('.tx-a').is(':visible') || t.closest('.answer-wrp').find('.tx-a').is(':visible') || (t.attr('data-type')==='without-answer' && t.find('input').val())){
			var o = audioPlayer[0];
			if(audioPlayer.attr('src')!==$(t.attr('data-target')).attr('data-src')){
				audioPlayer.attr('src',$(t.attr('data-target')).attr('data-src'))
				.off()
				.on('play',function(){
					t.addClass('playing');
				})
				.on('ended',function(){
					t.removeClass('playing');
				});
			}
			$('.audio-wrp').removeClass('open');
			$('.playing,.playing-script').removeClass('playing playing-script');
			t.addClass('playing');
			o.play();
		}
	});

	// video
	$('video').addClass('video-js');
	if($('#modalVideo').length){
		videojs(document.querySelector('.modal .video-js'),{
			controls:true,
			width:660,
			controlBar: {
				volumePanel: {inline: false}
			}
		},function onPlayerReady(){
			// role
			var role;
			$('.modal .video-js').append($('div.video-ctr-area'));
			$('.video-caption').parents('.video-js').find('video').on('timeupdate',function(){
				var o = $(this)[0],
					oJ = $(this);
				var time = o.currentTime;
				$('[data-toggle=video-caption]').each(function(){
					var t = $(this);
					var pt = String($(this).attr('data-playtime'));
					var playtime = pt.split(',')[0];
					var endtime = pt.split(',')[1];
					role = t.attr('data-role-org');
					if(playtime<time && time<endtime){
						t.addClass('playing-video-script');
						if(!role){
							return;
						}
						if(t.attr('data-role')===role){
							t.addClass('muted');
							if(!oJ.hasClass('manual-muted')){
								o.muted = true;
							}
						} else {
							t.removeClass('muted');
							if(!oJ.hasClass('manual-muted')){
								o.muted = false;
							}
						}
						return;
					} else {
						t.removeClass('playing-video-script');
					}
				});
			});
			$('button.vjs-mute-control').click(function(){
				var t = $(this),
					o = $('.video-js video')[0],
					oJ = $('.video-js video');
				if(o.muted){
					oJ.addClass('manual-muted');
				} else {
					oJ.removeClass('manual-muted');
				}
			});
			$('[data-toggle=video-caption-tg]').click(function(){
				$(this).toggleClass('on');
				$('.video-caption').toggle();
			});
			var videoCation = $('[data-toggle=video-caption]');
			$('[data-toggle=video-role]').click(function(){
				var t = $(this);
				var o = t.attr('data-role'),
					video = $('.video-js video');
					video.removeClass('manual-muted');
				if(!t.hasClass('on')){
					videoCation.attr('data-role-org',o);
					$('[data-toggle=video-role],[data-toggle=video-role-both]').removeClass('on');
					$('[data-toggle=video-role][data-role='+o+']').addClass('on');
				} else {
					$('[data-toggle=video-role]').removeClass('on');
					$('[data-toggle=video-role-both]').addClass('on');
					videoCation.removeAttr('data-role-org').removeClass('muted');
					video[0].muted = false;
				}
			});
			$('[data-toggle=video-role-both]').click(function(){
				$(this).prevAll('[data-toggle=video-role].on').click();
			});
			$('button.icon-modalvideo-role').click(function(){
				$(this).next().toggle();
			});
			this.on('ended',function(){
				$('[data-toggle=video-role-both]').click();
			});
			$('button.vjs-play-control').after('<button class="vjs-stop-control vjs-control vjs-button" type="button" aria-live="polite" title="Stop" aria-disabled="false"><span aria-hidden="true" class="vjs-icon-placeholder"></span>■</button>');
			$('button.vjs-stop-control').click(function(){
				var o = $(this).parents('.video-js').find('video')[0];
				o.pause();
				o.currentTime = 0;
			});
		});
	}
	if($('.video-area.sm').length){
		videojs(document.querySelector('.video-area.sm video'),{
			fluid:true,
			controls:true,
			controlBar: {
				volumePanel: {inline: false}
			}
		},function onPlayerReady(){
			this.on('ended', function(){

			});
		});
	}
	// voca
	$('button.btn-voca').click(function(){
		$(this).toggleClass('on');
	});
});

// audio
function audioStop(){
	"use strict";
	$('.audio-wrp').removeClass('open');
	$('.playing,.playing-script').removeClass('playing playing-script');
	var o = $('#audioPlayer')[0];
	if(!isNaN(o.duration)){
		o.pause();
		o.currentTime = 0;
	}
	var video = $('video')[0];
	if($('video').length && !isNaN(video.duration)){
		video.pause();
		video.currentTime = 0;
	}
}