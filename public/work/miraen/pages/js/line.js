$(function(){
	"use strict";
	$('.q-line').each(function(){
		$(this).prepend('<canvas id="canvas'+$(this).attr('id')+'" class="canvas"></canvas><button type="button" class="icon-re"><span class="sr-only">지우기</span></button>')
		.find('.q-line-btn').each(function(i){
			$(this).attr('data-no',i+1).append('<button type="button" data-toggle="q-line"><span class="sr-only">선택</span></button>');
		});
	});
	// 리사이즈 보정
	function appZoom(){			
		if(parent.ZOOMVALUE == undefined) {
			parent.ZOOMVALUE = 1;    			
		}
		return parent.ZOOMVALUE;
	}
	function offsetValue(value){
		return value/appZoom();
	}
	function qLine(o){
		o = $('#'+o);
		var btn = o.find('button[data-toggle=q-line]');
		var canvas = document.getElementById(o.find('canvas').attr('id'));
		var ctx;
		var sx, sy;
		var ex, ey;
		var drawing;
		var backup;
		var backupClear;
		var startGroup,startLine;
		canvas.width=o.width();
		canvas.height=o.height();
		ctx = canvas.getContext("2d");
		var a = $(o.attr('data-target'));
		a.data('line','');
		var userVal = [],
			userValIs = o.children('input.q-userline').val();
		var multi = o.attr('data-multiline');
		if(userValIs){
			userVal.push(userValIs);
		}
		o.on('touchmove',function(event){
			event.preventDefault();
			event.stopPropagation();
		});
		// 지우기
		o.find('.icon-re').css('z-index','1').on('click touchstart',function(){
			a.data('line','');
			a.removeClass('answered answer-o answer-x');
			o.find('.line-done').removeClass('line-done');
			o.removeClass('drawing no-touch');
			ctx.clearRect(0,0,canvas.width,canvas.height);
			backup='';
			backupClear='';
			o.find('input.q-userline').val('');
			userVal = [];
		});
		// 정답체크
		$(a.find('[data-toggle=answer-line]')).click(function(){
			var aD = a.attr('data-answer'),
				aL = a.data('line');
			if(a.hasClass('answered')){
				a.removeClass('answered answer-o answer-x');
				ctx.putImageData(backup, 0, 0);
				return;
			}
			a.addClass('answered');
			backup = ctx.getImageData(0, 0, canvas.width, canvas.height);
			var aLArray = aL.split(',').sort().toString();
			if(aD===aLArray){
				a.addClass('answer-o');
			} else {
				a.addClass('answer-x');
			}
			var aArray = aD.split(',').sort();
			for(var i=0;i<aArray.length;i++){
				aD = aArray[i];
				var aO = o.find('[data-no='+aD.split('-')[0]+']').children('button');
				var aO2 = o.find('[data-no='+aD.split('-')[1]+']').children('button');
				var aX = offsetValue(aO.offset().left-o.offset().left)+6;
				var aY = offsetValue(aO.offset().top-o.offset().top)+6;
				var aX2 = offsetValue(aO2.offset().left-o.offset().left)+6;
				var aY2 = offsetValue(aO2.offset().top-o.offset().top)+6;
				ctx.beginPath();
				ctx.moveTo(aX,aY);
				ctx.lineTo(aX2,aY2);
				ctx.strokeStyle = '#e15252';
				ctx.stroke();
				ctx.closePath();
			}
		});
		o.on('mousedown touchstart',function(e){
			var t;
			if($(e.target).is('button[data-toggle=q-line]')){
				t = $(e.target);
			} else {
				return;
			}
			if(a.hasClass('answered') || $(e.target).hasClass('line-done')){
				return false;
			}
			if(!multi){
				t.addClass('line-start');
			}
			startGroup = t.parent().attr('data-group');
			startLine = t.parent().attr('data-no');
			sx = offsetValue(t.offset().left-o.offset().left)+6;
			backup = ctx.getImageData(0, 0, canvas.width, canvas.height);
			sy = offsetValue(t.offset().top-o.offset().top)+6;
			backupClear = ctx.getImageData(0, 0, canvas.width, canvas.height);
			drawing = true;
			o.addClass('drawing');
		});
		o.on('mousemove touchmove',function(e){
			o.addClass('no-touch');
			e.preventDefault();
			ex = offsetValue(e.pageX-o.offset().left);
			ey = offsetValue(e.pageY-o.offset().top);
			if(e.touches){
				ex = offsetValue(e.touches[0].pageX-o.offset().left);
				ey = offsetValue(e.touches[0].pageY-o.offset().top);
			}
			if (drawing) {
				ctx.putImageData(backup, 0, 0);
				ctx.beginPath();
				ctx.moveTo(sx, sy);
				ctx.lineTo(ex, ey);
				ctx.strokeStyle = '#24201f';
				ctx.stroke();
				ctx.closePath();
			}
		});
		o.on('mouseup touchend',function(){
			if(a.hasClass('answered')){
				return false;
			}
			var t = $(this);
			btn.each(function(){
				var btnT = $(this);
				var btnTL = btnT.offset().left-o.offset().left;
				var btnTT = btnT.offset().top-o.offset().top;
				if(ex>offsetValue(btnTL-20) && ex<offsetValue(btnTL+20) && offsetValue(ey>btnTT-20) && ey<offsetValue(btnTT+20)){
					t = btnT;
					return false;
				}
			});
			if(!t.is('button[data-toggle=q-line]') || (!multi && (t.hasClass('line-done') || !o.find('.line-start').length))){
				ctx.putImageData(backupClear, 0, 0);
				drawing = false;
				o.find('.line-start').removeClass('line-start');
				o.removeClass('drawing no-touch');
				return;
			}
			var newAD = startLine+'-'+t.parent().attr('data-no');
			if(startLine>t.parent().attr('data-no')){
				newAD = t.parent().attr('data-no')+'-'+startLine;
			}
			if(a.data('line').indexOf(newAD)>-1 || (startGroup && startGroup===t.parent().attr('data-group'))){
				ctx.putImageData(backupClear, 0, 0);
			}  else {
				if(a.data('line')===''){
					a.data('line',newAD);
				} else {
					a.data('line',a.data('line')+','+newAD);
				}
				if(!multi){
					o.find('.line-start').addClass('line-done').removeClass('line-start');
					t.addClass('line-done');
				}
				ex = offsetValue(t.offset().left-o.offset().left)+6;
				ey = offsetValue(t.offset().top-o.offset().top)+6;
				ctx.putImageData(backup, 0, 0);
				ctx.beginPath();
				ctx.moveTo(sx, sy);
				ctx.lineTo(ex, ey);
				ctx.stroke();
				ctx.closePath();
				backupClear = ctx.getImageData(0, 0, canvas.width, canvas.height);
				// 사용자 입력 값
				userVal.push(newAD);
				o.children('input.q-userline').val(userVal);
			}
			drawing = false;
			o.removeClass('drawing no-touch');
		});
	}

	// 사용자 입력 값
	function userLine(o){
		o = $('#'+o);
		if(!o.find('input.q-userline').val()){
			return false;
		}
		var canvas = document.getElementById(o.find('canvas').attr('id'));
		var ctx;
		canvas.width=o.width();
		canvas.height=o.height();
		ctx = canvas.getContext("2d");

		var aD = o.children('input.q-userline').val();
		var aArray = aD.split(',').sort();
		for(var i=0;i<aArray.length;i++){
			aD = aArray[i];
			var aO = o.find('[data-no='+aD.split('-')[0]+']').children('button');
			var aO2 = o.find('[data-no='+aD.split('-')[1]+']').children('button');
			var aX = offsetValue(aO.offset().left-o.offset().left)+4;
			var aY = offsetValue(aO.offset().top-o.offset().top)+4;
			var aX2 = offsetValue(aO2.offset().left-o.offset().left)+4;
			var aY2 = offsetValue(aO2.offset().top-o.offset().top)+4;
			ctx.beginPath();
			ctx.moveTo(aX,aY);
			ctx.lineTo(aX2,aY2);
			ctx.stroke();
			ctx.closePath();
		}
		//o.find('.q-line-btn').addClass('line-done');
	}

	$('[data-toggle=answer-line-multi]').click(function(){
		var t = $(this);
		var o = t.attr('data-target');
		var a = $(o).find('[data-answer]');
		/*
		if(!a.find('#qLine .line-done').length || !a.find('#qLine2 .line-done').length){
			return false;
		}
		*/
		a.filter(':last').addClass('no-answered');
		a.find('[data-toggle=answer-line]').click();
		$(o).toggleClass('answered');
		if($(o).find('.answer-x').length){
			a.filter(':first').addClass('answer-x').removeClass('answer-o');
		} else if($(o).find('.answer-x').length===a.length){
			a.filter(':first').addClass('answer-o').removeClass('answer-x');
		}
	});
	var multiBtn = $('[data-toggle=answer-line-multi]').attr('data-target');
	$(multiBtn).addClass('answer-line-multi-wrp');
	$(multiBtn).children('button.icon-re').click(function(){
		$(multiBtn).find('.q-line button.icon-re').click();
		$($('[data-toggle=answer-line-multi]').attr('data-target')).removeClass('answered');
	});
	$('[data-toggle=answer-line-combi]').click(function(){
		var t = $(this);
		var o = $(t.attr('data-target'));
		t.next().click();
		o.find('button.icon-re').click(function(){
			o.find('.tx-a').hide();
		});
		if(o.hasClass('answered')){
			var tCorrect;
			o.find('.tx-a').show();
			if(o.hasClass('answer-x')){
				return false;
			}
			o.find('input[type=text]').each(function(){
				if($(this).val().toLowerCase()!==$(this).attr('data-answer').toLowerCase()){
					tCorrect = false;
					return false;
				}
				tCorrect = true;
			});
		} else {
			o.find('.tx-a').hide();
			o.find('input[type=text]').val('');
		}
	});
	// 실행
	setTimeout(function(){
		$('.q-line').each(function(){
			qLine($(this).attr('id'));
		});
	},500);
	setTimeout(function(){
		$('.q-line').each(function(){
			userLine($(this).attr('id'));
		});
	},750);
});