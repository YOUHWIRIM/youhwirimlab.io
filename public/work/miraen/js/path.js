$(function(){
	$('#root_tree').on('click', 'a', function (e) {
		if (!e.ctrlKey) { //ctrlKey
			e.preventDefault();
			var href = $(this).attr('href');
			if (href != 'javascript:;') {
				$('#viewArea').find('iframe').attr('src', href);
				$('#url').val(href);
			}
		}
	});
	//filter
	$('#filter').treeListFilter('#root_tree', 200);

	$('#url').on('keypress', function (e) {
		if (e.keyCode == '13') {
			var url = $(this).val();
			$('#root_tree').find('a').each(function () {
				var href = $(this).attr('href');
				if (href == url) {
					$(this).click();
				}
			});
		}
	});

	$('#root_tree li').each(function () {
		var comment = $(this).children('dl').find('dd').length;
		if (comment > 0) {
			$(this).append('<span class="view-comment"><i class="fa fa-plus-circle" aria-hidden="true"><span>수정</span></i></span>');
			$('.view-comment').bind('click', function () {
				$(this).hide().prev('dl').slideDown();
			});
		}
	});
	$('#root_tree a').each(function () {
		var $this = $(this);
		var url = $this.attr('href');
		if ($this.next('ul').length > 0) {
			$this.addClass('close');
		}
		if (url == 'javascript:;' || url == '') {
			$this.on('click', function () {
				var $list = $this.next('ul');
				if (!$list.is(':visible')) {
					$list.slideDown();
					$this.removeClass('open').addClass('close');
				} else {
					$list.slideUp();
					$this.removeClass('close').addClass('open');
				}
			});
		} else {
			$this.attr('title', '전체화면을 보시려면 Ctrl + 클릭');
		}
	});

	//반응형
	var $viewArea = $('#viewArea');
	var $responsiveBtn = $('.view-btn');

	if($responsiveBtn.length > 0){
		$responsiveBtn.on('click','button',function(){
			$viewArea.removeClass().addClass($(this).attr('class'));
			
		});
	}	
});