$(document).ready(function(){
     $('#menu').mouseenter(function() {
      $(this).children('li').eq(0).css({ 'background-color': '#fff' })
      $(this).children('li').eq(1).delay(100).css({ 'background-color': '#fff' })
      $(this).children('li').eq(2).delay(200).css({ 'background-color': '#fff' })
    })
    $('#menu').mouseleave(function() {
      $('#menu li:nth-child(3)').css({ 'background-color': '' })
      $('#menu li:nth-child(2)').delay(200).css({ 'background-color': '' })
      $('#menu li:nth-child(1)').delay(400).css({ 'background-color': '' })
    })

    $('#section1_text').show()
    //초기화하기


    //li버튼 클릭할 때마다 dnc
    $('#slide_bt li').click(function() {

      //배경전환
      bg_mv = $(this).index() * -1430
      $('#wrap_bg').animate({ left: bg_mv }, 600)

      slide_num = bg_mv


      //버튼 이미지 바뀜
      bt_num = $(this).index()
      $(this).children().attr('src', '1page/select_bt0.png')
      $(this).siblings().children().attr('src', '1page/select_bt1.png')

      //now text의 opacity 바뀜
      $('#slide_now_text li').eq(bt_num).css({ 'opacity': '1' })
      $('#slide_now_text li').eq(bt_num).siblings().css({ 'opacity': '0' })



      //ex_text 내용 바뀜

      //11111               
      if (bt_num == 0) {
        $('#ex_text p').html('Main page. Announce the beginning of a site.')
        $('#left p').html('FIRST PAGE')
        $('#right p').html('NEXT<b>02</b>')

        //section1 초기화값
        $('#section1_text').css({ 'display': 'none' })
        $('#section1_text img').stop()
        $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
        $('#section1_text p').stop();
        $('#section1_text p').css({ 'opacity': '0' })
        $('#section1_text h1').css({ 'display': 'none' })
        $('#section1_text h5').css({ 'opacity': '0' })
        $('#button').stop();
        $('#button').css({ 'opacity': '0' })



        //section1 켜줌   
        $('#arrow_wrap #left').css({ 'display': 'none' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section1_text').show()

        $('#section1_text').animate({ 'opacity': '1', 'right': '0px' }, 500, function() {
          //section1 실행
          $('#section1_text img').show()
          $('#section1_text img').stop().animate({ 'opacity': '1' }, 700, function() {
            $('#section1_text p').animate({ 'opacity': '1' }, 700, function() {
              $('#section1_text h1').slideDown(2300)
              $('#section1_text h5').delay(1000).animate({ 'opacity': '1' }, 800)
              $('#button').delay(1500).animate({ 'opacity': '1' }, 1000)
            })
          })

        })

        //1번 클릭하면 2번이 사라지는 동작 - 콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 300, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //1번 클릭하면 3번이 사라지는 동작 - 콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //1번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4').css({ 'display': 'none' })
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //1번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)


        //1번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //1번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)






      }
      //22222
      else if (bt_num == 1) {
        $('#ex_text p').html('A brief introduction of the night sky on the site.')
        $('#left p').html('PREV <b>01</b>')
        $('#right p').html('NEXT <b>03</b>')



        //section2 초기화값
        $('#section2').css({ 'opacity': '0', 'right': '0px' })
        $('#section2').css({ 'display': 'none' })
        $('#section2_text').css({ 'opacity': '0' })
        $('#section2_text h2').css({ 'opacity': '0', 'right': '0px' })
        $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
        $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' })


        //section2 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })

        $('#section2').show()
        $('#section2').animate({ 'opacity': '1' }, 300, function() {
          //                        section2 실행
          $('#section2_text').animate({ 'opacity': '1' }, 800)
          $('#section2_text h2').animate({ 'opacity': '1', 'right': '0px' }, 800)
          $('#section2_text p').delay(100).animate({ 'opacity': '1', 'right': '0px' }, 800, function() {
            $('#section2_img_wrap0').animate({ 'width': '880px', 'opacity': '1' }, 1200)
          })
        })


        //2번 클릭하면 1번이 사라지는 동작 - 콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //2번 클릭하면 3번이 사라지는 동작 - 콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //2번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //2번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)


        //2번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //2번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)




      }

      //33333
      else if (bt_num == 2) {
        $('#ex_text p').html('An introduction of constellation month to month.')
        $('#left p').html('PREV <b>02</b>')
        $('#right p').html('NEXT <b>04</b>')



        //section3 초기화값
        $('#section3').css({ 'display': 'none' })
        $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        $('#section3_slide_wrap').css({ 'opacity': '0' })
        $('#section3_text_bt').css({ 'display': 'none' })
        //                  $('#slide_left_img_wrap img').css({'opacity':'0'})
        //                  $('#slide_right_img_wrap img').css({'opacity':'0'})
        $('#section3_text1').css({ 'top': '10px', 'opacity': '0' })
        $('#section3_text2').css({ 'bottom': '-10px', 'opacity': '0' })



        //section3 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section3').show()
        $('#section3_wrap').animate({ 'opacity': '1' }, 300, function() {

          $('#section3_slide_wrap').delay(200).animate({ 'opacity': '1' }, 700)
          $('#section3_text_bt').delay(200).fadeIn(700)

          $('#section3_text1').delay(500).animate({ 'top': '0px', 'opacity': '1' }, 800)
          $('#section3_text2').delay(600).animate({ 'bottom': '0px', 'opacity': '1' }, 800)
        })

        //3번 클릭하면 1번이 사라지는 동작 - 콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //3번 클릭하면 2번이 사라지는 동작 - 콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })

        //3번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //3번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //3번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //3번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)




      }

      //44444444444
      else if (bt_num == 3) {
        $('#ex_text p').html('More information about constellations.')
        $('#left p').html('PREV <b>03</b>')
        $('#right p').html('NEXT <b>05</b>')


        //section4 초기화값
        $('#section4').css({ 'display': 'none' })

        $('#section4_header').stop()
        $('#section4_first_wrap').stop()
        $('#section4_first_text1').stop()
        $('#section4_first_text2').stop()
        $('#section4_first_text3').stop()
        $('#section4_first_text4').stop()
        $('#section4_first_text1').children('div').stop()
        $('#section4_first_text2').children('div').stop()
        $('#section4_first_text3').children('div').stop()
        $('#section4_first_text4').children('div').stop()
        $('#section4_first_text1').children('img').stop()

        $('#section4_header').css({ 'opacity': '0' })
        $('#section4_first_wrap').css({ 'opacity': '0' })
        $('#section4_first_text1').css({ 'height': '0%' })
        $('#section4_first_text2').css({ 'height': '0%' })
        $('#section4_first_text3').css({ 'height': '0%' })
        $('#section4_first_text4').css({ 'height': '0%' })
        $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text1').children('img').css({ 'opacity': '0' })




        //section4 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeIn()
        $('#section4_header').animate({ 'opacity': '1' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '1', 'left': '0px' }, 100)
        $('#section4_first_text1').delay(1100).animate({ 'height': '100%' }, 400)
        $('#section4_first_text2').delay(1400).animate({ 'height': '100%' }, 400)
        $('#section4_first_text3').delay(1700).animate({ 'height': '100%' }, 400)
        $('#section4_first_text4').delay(2000).animate({ 'height': '100%' }, 400, function() {
          $('#section4_first_text1').children('div').animate({ 'left': '20px', 'opacity': '1' }, 400)
          $('#section4_first_text2').children('div').delay(200).animate({ 'left': '20px', 'opacity': '1' }, 400)
          $('#section4_first_text3').children('div').delay(400).animate({ 'left': '20px', 'opacity': '1' }, 400)
          $('#section4_first_text4').children('div').delay(600).animate({ 'left': '20px', 'opacity': '1' }, 400, function() {
            $('#section4_first_text1').children('img').delay(300).animate({ 'opacity': '0.5' }, 1500)
          })
        })



        //4번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //4번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //4번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //4번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //4번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //4번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)




      }

      //555555555555
      else if (bt_num == 4) {
        $('#ex_text p').html('More information about aurora.')
        $('#left p').html('PREV <b>04</b>')
        $('#right p').html('NEXT <b>06</b>')





        //section5 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section5').fadeIn(500)
        $('#section5_header').delay(700).animate({ 'opacity': '1' }, 800)




        //5번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //5번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //5번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //5번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })


        //5번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //5번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)


      }




      //666666666666
      else if (bt_num == 5) {
        $('#ex_text p').html('More images and videos of aurora.')
        $('#left p').html('PREV <b>05</b>')
        $('#right p').html('NEXT <b>07</b>')


        //section6 초기화값


        //section6 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section6').fadeIn(1000)


        //6번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //6번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //6번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //6번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //6번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //6번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)



      }

      //777777777777
      else if (bt_num == 6) {
        $('#ex_text p').html('Observation place of the night sky, conditions, tips, and other contents.')
        $('#left p').html('PREV <b>06</b>')
        $('#right p').html('LAST PAGE')


        //section7 켜줌
        $('#section7').fadeIn(500)
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'none' })
        $('#section7_text p').delay(800).animate({ 'opacity': '1' }, 700)
        $('#section7_text h2').delay(1300).slideDown(1200)
        $('#section7_spot').delay(1150).fadeIn(1000)
        $('#section7_spot_click div').delay(1300).fadeIn(1000)

        //section7 초기화
        $('#section7_text p').css({ 'opacity': '0' })
        $('#section7_text h2').css({ 'display': 'none' })
        $('#section7_spot').css({ 'display': 'none' })
        $('#section7_spot_click div').css({ 'display': 'none' })




        $('#typewriteText').typewrite({
          actions: [
            { delay: 3000 },
            { type: ' ' },
            { type: 'BY PRESSING THIS BUTTON ' },
            { type: '<br>' },
            { type: 'DESCRIPTION PAGE WILL APPEAR. ' },
          ]
        });



        //7번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //7번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //7번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //7번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //7번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //7번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)






      }

    })
    //--------------------------------------------------------------------------------------------------
    //left 클릭할 때마다
    slide_num = 0;
    li_num = +1430;

    $('#left').click(function() {

      slide_num = slide_num + 1430
      $('#wrap_bg').animate({ left: slide_num }, 600)



      li_num = slide_num / -1430;
      //li이미지 바뀜
      $('#slide_bt li').eq(li_num).children().attr('src', '1page/select_bt0.png')
      $('#slide_bt li').eq(li_num).siblings().children().attr('src', '1page/select_bt1.png')
      //now_text opacity 바뀜
      $('#slide_now_text li').eq(li_num).css({ 'opacity': '1' })
      $('#slide_now_text li').eq(li_num).siblings().css({ 'opacity': '0' })



      //ex_text 내용 바뀜
      if (li_num == 0) {
        $('#ex_text p').html('Main page. Announce the beginning of a site.')
        $('#left p').html('FIRST PAGE')
        $('#right p').html('NEXT<b>02</b>')

        //section1 초기화값
        $('#section1_text').css({ 'display': 'none' })
        $('#section1_text img').stop()
        $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
        $('#section1_text p').stop();
        $('#section1_text p').css({ 'opacity': '0' })
        $('#section1_text h1').css({ 'display': 'none' })
        $('#section1_text h5').css({ 'opacity': '0' })
        $('#button').stop();
        $('#button').css({ 'opacity': '0' })



        //section1 켜줌   
        $('#arrow_wrap #left').css({ 'display': 'none' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section1_text').show()

        $('#section1_text').animate({ 'opacity': '1', 'right': '0px' }, 500, function() {
          //section1 실행

          $('#section1_text img').show()
          $('#section1_text img').stop().animate({ 'opacity': '1' }, 700, function() {
            $('#section1_text p').animate({ 'opacity': '1' }, 700, function() {
              $('#section1_text h1').slideDown(2300)
              $('#section1_text h5').delay(1000).animate({ 'opacity': '1' }, 800)
              $('#button').delay(1500).animate({ 'opacity': '1' }, 1000)
            })
          })

        })

        //1번 클릭하면 2번이 사라지는 동작 - 콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 300, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //1번 클릭하면 3번이 사라지는 동작 - 콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //1번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4').css({ 'display': 'none' })
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //1번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)


        //1번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //1번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)



      } else if (li_num == 1) {
        $('#ex_text p').html('A brief introduction of the night sky on the site.')
        $('#left p').html('PREV <b>01</b>')
        $('#right p').html('NEXT <b>03</b>')



        //section2 초기화값
        $('#section2').css({ 'opacity': '0', 'right': '0px' })
        $('#section2').css({ 'display': 'none' })
        $('#section2_text').css({ 'opacity': '0' })
        $('#section2_text h2').css({ 'opacity': '0', 'right': '0px' })
        $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
        $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' })


        //section2 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })

        $('#section2').show()
        $('#section2').animate({ 'opacity': '1' }, 300, function() {
          //                        section2 실행
          $('#section2_text').animate({ 'opacity': '1' }, 800)
          $('#section2_text h2').animate({ 'opacity': '1', 'right': '0px' }, 800)
          $('#section2_text p').delay(100).animate({ 'opacity': '1', 'right': '0px' }, 800, function() {
            $('#section2_img_wrap0').animate({ 'width': '880px', 'opacity': '1' }, 1200)
          })
        })


        //2번 클릭하면 1번이 사라지는 동작 - 콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //2번 클릭하면 3번이 사라지는 동작 - 콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //2번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //2번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)


        //2번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //2번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)


      } else if (li_num == 2) {
        $('#ex_text p').html('An introduction of constellation month to month.')
        $('#left p').html('PREV <b>02</b>')
        $('#right p').html('NEXT <b>04</b>')



        //section3 초기화값
        $('#section3').css({ 'display': 'none' })
        $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        $('#section3_slide_wrap').css({ 'opacity': '0' })
        $('#section3_text_bt').css({ 'display': 'none' })
        $('#section3_text1').css({ 'top': '10px', 'opacity': '0' })
        $('#section3_text2').css({ 'bottom': '-10px', 'opacity': '0' })



        //section3 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section3').show()
        $('#section3_wrap').animate({ 'opacity': '1' }, 300, function() {

          $('#section3_slide_wrap').delay(200).animate({ 'opacity': '1' }, 700)
          $('#section3_text_bt').delay(200).fadeIn(700)

          $('#section3_text1').delay(500).animate({ 'top': '0px', 'opacity': '1' }, 800)
          $('#section3_text2').delay(600).animate({ 'bottom': '0px', 'opacity': '1' }, 800)
        })

        //3번 클릭하면 1번이 사라지는 동작 - 콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //3번 클릭하면 2번이 사라지는 동작 - 콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })

        //3번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //3번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //3번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //3번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)


      } else if (li_num == 3) {
        $('#ex_text p').html('More information about constellations.')
        $('#left p').html('PREV <b>03</b>')
        $('#right p').html('NEXT <b>05</b>')


        //section4 초기화값
        $('#section4').css({ 'display': 'none' })

        $('#section4_header').stop()
        $('#section4_first_wrap').stop()
        $('#section4_first_text1').stop()
        $('#section4_first_text2').stop()
        $('#section4_first_text3').stop()
        $('#section4_first_text4').stop()
        $('#section4_first_text1').children('div').stop()
        $('#section4_first_text2').children('div').stop()
        $('#section4_first_text3').children('div').stop()
        $('#section4_first_text4').children('div').stop()
        $('#section4_first_text1').children('img').stop()

        $('#section4_header').css({ 'opacity': '0' })
        $('#section4_first_wrap').css({ 'opacity': '0' })
        $('#section4_first_text1').css({ 'height': '0%' })
        $('#section4_first_text2').css({ 'height': '0%' })
        $('#section4_first_text3').css({ 'height': '0%' })
        $('#section4_first_text4').css({ 'height': '0%' })
        $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text1').children('img').css({ 'opacity': '0' })




        //section4 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeIn()
        $('#section4_header').animate({ 'opacity': '1' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '1', 'left': '0px' }, 100)
        $('#section4_first_text1').delay(1100).animate({ 'height': '100%' }, 400)
        $('#section4_first_text2').delay(1400).animate({ 'height': '100%' }, 400)
        $('#section4_first_text3').delay(1700).animate({ 'height': '100%' }, 400)
        $('#section4_first_text4').delay(2000).animate({ 'height': '100%' }, 400, function() {
          $('#section4_first_text1').children('div').animate({ 'left': '20px', 'opacity': '1' }, 400)
          $('#section4_first_text2').children('div').delay(200).animate({ 'left': '20px', 'opacity': '1' }, 400)
          $('#section4_first_text3').children('div').delay(400).animate({ 'left': '20px', 'opacity': '1' }, 400)
          $('#section4_first_text4').children('div').delay(600).animate({ 'left': '20px', 'opacity': '1' }, 400, function() {
            $('#section4_first_text1').children('img').delay(300).animate({ 'opacity': '0.5' }, 1500)
          })
        })



        //4번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //4번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //4번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //4번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //4번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //4번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)

      } else if (li_num == 4) {
        $('#ex_text p').html('More information about aurora.')
        $('#left p').html('PREV <b>04</b>')
        $('#right p').html('NEXT <b>06</b>')





        //section5 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section5').fadeIn(500)
        $('#section5_header').delay(700).animate({ 'opacity': '1' }, 800)




        //5번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //5번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //5번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //5번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })


        //5번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //5번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)

      } else if (li_num == 5) {
        $('#ex_text p').html('More images and videos of aurora.')
        $('#left p').html('PREV <b>05</b>')
        $('#right p').html('NEXT <b>07</b>')


        //section6 초기화값


        //section6 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section6').fadeIn(1000)


        //6번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //6번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //6번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //6번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //6번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //6번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)
      } else if (li_num == 6) {
        $('#ex_text p').html('Observation place of the night sky, conditions, tips, and other contents.')
        $('#left p').html('PREV <b>06</b>')
        $('#right p').html('LAST PAGE')


        //section7 켜줌
        $('#section7').fadeIn(500)
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'none' })
        $('#section7_text p').delay(800).animate({ 'opacity': '1' }, 700)
        $('#section7_text h2').delay(1300).slideDown(1200)
        $('#section7_spot').delay(1150).fadeIn(1000)
        $('#section7_spot_click div').delay(1300).fadeIn(1000)

        //section7 초기화
        $('#section7_text p').css({ 'opacity': '0' })
        $('#section7_text h2').css({ 'display': 'none' })
        $('#section7_spot').css({ 'display': 'none' })
        $('#section7_spot_click div').css({ 'display': 'none' })




        $('#typewriteText').typewrite({
          actions: [
            { delay: 3000 },
            { type: ' ' },
            { type: 'BY PRESSING THIS BUTTON ' },
            { type: '<br>' },
            { type: 'DESCRIPTION PAGE WILL APPEAR. ' },
          ]
        });



        //7번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //7번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //7번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //7번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //7번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //7번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)



      }

    })



    //--------------------------------------------------------------------------------------------------
    //right 클릭할 때마다
    slide_num = 0;
    li_num = -1430;

    $('#right').click(function() {

      slide_num = slide_num - 1430
      $('#wrap_bg').animate({ left: slide_num }, 600)



      li_num = slide_num / -1430;
      //li이미지 바뀜
      $('#slide_bt li').eq(li_num).children().attr('src', '1page/select_bt0.png')
      $('#slide_bt li').eq(li_num).siblings().children().attr('src', '1page/select_bt1.png')
      //now_text opacity 바뀜
      $('#slide_now_text li').eq(li_num).css({ 'opacity': '1' })
      $('#slide_now_text li').eq(li_num).siblings().css({ 'opacity': '0' })



      //ex_text 내용 바뀜
      if (li_num == 0) {
        $('#ex_text p').html('Main page. Announce the beginning of a site.')
        $('#left p').html('FIRST PAGE')
        $('#right p').html('NEXT<b>02</b>')

        //section1 초기화값
        $('#section1_text').css({ 'display': 'none' })
        $('#section1_text img').stop()
        $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
        $('#section1_text p').stop();
        $('#section1_text p').css({ 'opacity': '0' })
        $('#section1_text h1').css({ 'display': 'none' })
        $('#section1_text h5').css({ 'opacity': '0' })
        $('#button').stop();
        $('#button').css({ 'opacity': '0' })



        //section1 켜줌   
        $('#arrow_wrap #left').css({ 'display': 'none' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section1_text').show()

        $('#section1_text').animate({ 'opacity': '1', 'right': '0px' }, 500, function() {
          //section1 실행
          $('#section1_text img').show()
          $('#section1_text img').stop().animate({ 'opacity': '1' }, 700, function() {
            $('#section1_text p').animate({ 'opacity': '1' }, 700, function() {
              $('#section1_text h1').slideDown(2300)
              $('#section1_text h5').delay(1000).animate({ 'opacity': '1' }, 800)
              $('#button').delay(1500).animate({ 'opacity': '1' }, 1000)
            })
          })

        })

        //1번 클릭하면 2번이 사라지는 동작 - 콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 300, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //1번 클릭하면 3번이 사라지는 동작 - 콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //1번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4').css({ 'display': 'none' })
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //1번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)


        //1번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //1번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)



      } else if (li_num == 1) {
        $('#ex_text p').html('A brief introduction of the night sky on the site.')
        $('#left p').html('PREV <b>01</b>')
        $('#right p').html('NEXT <b>03</b>')



        //section2 초기화값
        $('#section2').css({ 'opacity': '0', 'right': '0px' })
        $('#section2').css({ 'display': 'none' })
        $('#section2_text').css({ 'opacity': '0' })
        $('#section2_text h2').css({ 'opacity': '0', 'right': '0px' })
        $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
        $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' })


        //section2 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })

        $('#section2').show()
        $('#section2').animate({ 'opacity': '1' }, 300, function() {
          //                        section2 실행
          $('#section2_text').animate({ 'opacity': '1' }, 800)
          $('#section2_text h2').animate({ 'opacity': '1', 'right': '0px' }, 800)
          $('#section2_text p').delay(100).animate({ 'opacity': '1', 'right': '0px' }, 800, function() {
            $('#section2_img_wrap0').animate({ 'width': '880px', 'opacity': '1' }, 1200)
          })
        })


        //2번 클릭하면 1번이 사라지는 동작 - 콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //2번 클릭하면 3번이 사라지는 동작 - 콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //2번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //2번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)


        //2번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //2번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)


      } else if (li_num == 2) {
        $('#ex_text p').html('An introduction of constellation month to month.')
        $('#left p').html('PREV <b>02</b>')
        $('#right p').html('NEXT <b>04</b>')



        //section3 초기화값
        $('#section3').css({ 'display': 'none' })
        $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        $('#section3_slide_wrap').css({ 'opacity': '0' })
        $('#section3_text_bt').css({ 'display': 'none' })
        $('#section3_text1').css({ 'top': '10px', 'opacity': '0' })
        $('#section3_text2').css({ 'bottom': '-10px', 'opacity': '0' })



        //section3 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section3').show()
        $('#section3_wrap').animate({ 'opacity': '1' }, 300, function() {

          $('#section3_slide_wrap').delay(200).animate({ 'opacity': '1' }, 700)
          $('#section3_text_bt').delay(200).fadeIn(700)

          $('#section3_text1').delay(500).animate({ 'top': '0px', 'opacity': '1' }, 800)
          $('#section3_text2').delay(600).animate({ 'bottom': '0px', 'opacity': '1' }, 800)
        })

        //3번 클릭하면 1번이 사라지는 동작 - 콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //3번 클릭하면 2번이 사라지는 동작 - 콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })

        //3번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //3번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //3번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //3번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)


      } else if (li_num == 3) {
        $('#ex_text p').html('More information about constellations.')
        $('#left p').html('PREV <b>03</b>')
        $('#right p').html('NEXT <b>05</b>')


        //section4 초기화값
        $('#section4').css({ 'display': 'none' })

        $('#section4_header').stop()
        $('#section4_first_wrap').stop()
        $('#section4_first_text1').stop()
        $('#section4_first_text2').stop()
        $('#section4_first_text3').stop()
        $('#section4_first_text4').stop()
        $('#section4_first_text1').children('div').stop()
        $('#section4_first_text2').children('div').stop()
        $('#section4_first_text3').children('div').stop()
        $('#section4_first_text4').children('div').stop()
        $('#section4_first_text1').children('img').stop()

        $('#section4_header').css({ 'opacity': '0' })
        $('#section4_first_wrap').css({ 'opacity': '0' })
        $('#section4_first_text1').css({ 'height': '0%' })
        $('#section4_first_text2').css({ 'height': '0%' })
        $('#section4_first_text3').css({ 'height': '0%' })
        $('#section4_first_text4').css({ 'height': '0%' })
        $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '1' }, 400)
        $('#section4_first_text1').children('img').css({ 'opacity': '0' })




        //section4 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeIn()
        $('#section4_header').animate({ 'opacity': '1' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '1', 'left': '0px' }, 100)
        $('#section4_first_text1').delay(1100).animate({ 'height': '100%' }, 400)
        $('#section4_first_text2').delay(1400).animate({ 'height': '100%' }, 400)
        $('#section4_first_text3').delay(1700).animate({ 'height': '100%' }, 400)
        $('#section4_first_text4').delay(2000).animate({ 'height': '100%' }, 400, function() {
          $('#section4_first_text1').children('div').animate({ 'left': '20px', 'opacity': '1' }, 400)
          $('#section4_first_text2').children('div').delay(200).animate({ 'left': '20px', 'opacity': '1' }, 400)
          $('#section4_first_text3').children('div').delay(400).animate({ 'left': '20px', 'opacity': '1' }, 400)
          $('#section4_first_text4').children('div').delay(600).animate({ 'left': '20px', 'opacity': '1' }, 400, function() {
            $('#section4_first_text1').children('img').delay(300).animate({ 'opacity': '0.5' }, 1500)
          })
        })



        //4번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //4번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //4번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //4번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //4번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //4번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)

      } else if (li_num == 4) {
        $('#ex_text p').html('More information about aurora.')
        $('#left p').html('PREV <b>04</b>')
        $('#right p').html('NEXT <b>06</b>')





        //section5 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section5').fadeIn(500)
        $('#section5_header').delay(700).animate({ 'opacity': '1' }, 800)




        //5번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //5번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //5번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //5번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })


        //5번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)

        //5번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)
      } else if (li_num == 5) {
        $('#ex_text p').html('More images and videos of aurora.')
        $('#left p').html('PREV <b>05</b>')
        $('#right p').html('NEXT <b>07</b>')


        //section6 초기화값


        //section6 켜줌
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'block' })
        $('#section6').fadeIn(1000)


        //6번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //6번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //6번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //6번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //6번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //6번클릭하면 7번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section7').fadeOut(500)

      } else if (li_num == 6) {
        $('#ex_text p').html('Observation place of the night sky, conditions, tips, and other contents.')
        $('#left p').html('PREV <b>06</b>')
        $('#right p').html('LAST PAGE')


        //section7 켜줌
        $('#section7').fadeIn(500)
        $('#arrow_wrap #left').css({ 'display': 'block' })
        $('#arrow_wrap #right').css({ 'display': 'none' })
        $('#section7_text p').delay(800).animate({ 'opacity': '1' }, 700)
        $('#section7_text h2').delay(1300).slideDown(1200)
        $('#section7_spot').delay(1150).fadeIn(1000)
        $('#section7_spot_click div').delay(1300).fadeIn(1000)

        //section7 초기화
        $('#section7_text p').css({ 'opacity': '0' })
        $('#section7_text h2').css({ 'display': 'none' })
        $('#section7_spot').css({ 'display': 'none' })
        $('#section7_spot_click div').css({ 'display': 'none' })




        $('#typewriteText').typewrite({
          actions: [
            { delay: 3000 },
            { type: ' ' },
            { type: 'BY PRESSING THIS BUTTON ' },
            { type: '<br>' },
            { type: 'DESCRIPTION PAGE WILL APPEAR. ' },
          ]
        });



        //7번클릭하면 1번이 사라지는 동작-콜백 ( 1번 css 초기값 )
        $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section1_text').css({ 'display': 'none' })
          $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
          $('#section1_text p').css({ 'opacity': '0' })
          $('#section1_text h1').css({ 'display': 'none' })
          $('#section1_text h5').css({ 'opacity': '0' })
          $('#button').css({ 'opacity': '0' })
        })


        //7번클릭하면 2번이 사라지는 동작-콜백 ( 2번 css 초기값 )
        $('#section2').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section2').css({ 'display': 'none' })
          $('#section2_text').css({ 'opacity': '0' })
          $('#section2_text h2').css({ 'opacity': '0', 'right': '-50px' })
          $('#section2_text p').css({ 'opacity': '0', 'right': '-30px' })
          $('#section2_img_wrap0').css({ 'width': '0px', 'opacity': '0' }, 1200)
        })


        //7번클릭하면 3번이 사라지는 동작-콜백 ( 3번 css 초기값 )
        $('#section3_wrap').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
          $('#section3').css({ 'display': 'none' })
          $('#section3_wrap').css({ 'opacity': '0', 'right': '0' })
        })

        //7번 클릭하면 4번이 사라지는 동작 - 콜백 ( 4번 css 초기값)
        $('#arrow_wrap').animate({ 'opacity': '1' }, 500)
        $('#section4').fadeOut()
        $('#section4_header').animate({ 'opacity': '0' }, 500)
        $('#section4_first_wrap').animate({ 'opacity': '0', 'left': '-100px' }, 500, function() {
          $('#section4_first_text1').css({ 'height': '0%' })
          $('#section4_first_text2').css({ 'height': '0%' })
          $('#section4_first_text3').css({ 'height': '0%' })
          $('#section4_first_text4').css({ 'height': '0%' })
          $('#section4_first_text1').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text2').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text3').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text4').children('div').css({ 'left': '-150px', 'opacity': '0' })
          $('#section4_first_text1').children('img').css({ 'opacity': '0' })
        })

        //7번클릭하면 5번이 사라지는 동작-콜백 ( 5번 css 초기값 )
        $('#section5').fadeOut(500)

        //7번 클릭하면 6번이 사라지는 동작 - 콜백 ( 6번 css 초기값)
        $('#section6').fadeOut(500)



      }

    })






    //------------------------------------------------------------------------------------------------
    //1page의 button 클릭할 때 2page로 전환
    $('#button').click(function() {
      slide_num = slide_num - 1430
      $('#wrap_bg').animate({ left: slide_num }, 600)

      //버튼 클릭하면 1번이 사라지는 동작 - 콜백 ( 1번 css 초기값 )

      //section2 켜줌
      $('#arrow_wrap #left').css({ 'display': 'block' })
      $('#arrow_wrap #right').css({ 'display': 'block' })

      $('#section2').show()
      $('#section2').animate({ 'opacity': '1' }, 300, function() {
        //                      section2 실행
        $('#section2_text').animate({ 'opacity': '1' }, 800)
        $('#section2_text h2').animate({ 'opacity': '1', 'right': '0px' }, 800)
        $('#section2_text p').delay(100).animate({ 'opacity': '1', 'right': '0px' }, 800, function() {
          $('#section2_img_wrap0').animate({ 'width': '880px', 'opacity': '1' }, 1200)
        })
      })


      //버튼 클릭하면 1번이 사라지는 동작 - 콜백 ( 1번 css 초기값 )
      $('#section1_text').animate({ 'opacity': '0', 'right': '100px' }, 500, function() {
        $('#section1_text').css({ 'display': 'none' })
        $('#section1_text img').css({ 'display': 'none', 'opacity': '0' })
        $('#section1_text p').css({ 'opacity': '0' })
        $('#section1_text h1').css({ 'display': 'none' })
        $('#section1_text h5').css({ 'opacity': '0' })
        $('#button').css({ 'opacity': '0' })
      })

      $('#left p').html('FIRST PAGE')
      $('#right p').html('NEXT <b>03</b>')

      $('#ex_text p').html('A brief introduction of the night sky on the site.')

      $('#slide_now_text li').eq(1).css({ 'opacity': '1' })
      $('#slide_now_text li').eq(1).siblings().css({ 'opacity': '0' })

      $('#slide_bt li:eq(1)').children().attr('src', '1page/select_bt0.png')
      $('#slide_bt li:eq(1)').siblings().children().attr('src', '1page/select_bt1.png')


    })
    //------------------------------------------------------------------------------------------------
    //  section1 내용 애니메이트


    $('.guideBg div button').click(function(){
      $('.guideBg').fadeOut(500,function(){
        $('#section1_text img').delay(0).animate({ 'opacity': '1' }, 700, function() {
          $('#section1_text p').animate({ 'opacity': '1' }, 700, function() {
            $('#section1_text h1').slideDown(2300)
            $('#section1_text h5').delay(1000).animate({ 'opacity': '1' }, 800)
            $('#button').delay(1500).animate({ 'opacity': '1' }, 1000)
          })
        })
      })
    })


    //-----------------------------------------------------------------------------------------------
    //section2 내용
    $('.section2_img').mouseenter(function() {
      $(this).children('#line').animate({ 'opacity': '0.5' })

      $(this).children('#section2_img_text').children('h5').animate({ 'bottom': '215px', 'opacity': '1' }, 500)
      $(this).children('#section2_img_text').children('p').delay(100).animate({ 'bottom': '160px', 'opacity': '1' }, 300)

      $(this).children('#line').children('div').eq(0).stop().animate({ 'right': '20px' }, 500)
      $(this).children('#line').children('div').eq(1).stop().animate({ 'top': '20px' }, 500)
      $(this).children('#line').children('div').eq(2).stop().animate({ 'left': '20px' }, 500)
      $(this).children('#line').children('div').eq(3).stop().animate({ 'bottom': '20px' }, 500)

      $(this).children('#line').children('div').eq(4).stop().animate({ 'right': '280px' }, 500)
      $(this).children('#line').children('div').eq(5).stop().animate({ 'top': '380px' }, 500)
      $(this).children('#line').children('div').eq(6).stop().animate({ 'left': '280px' }, 500)
      $(this).children('#line').children('div').eq(7).stop().animate({ 'bottom': '380px' }, 500)
    })

    $('.section2_img').mouseleave(function() {
      $(this).children('#line').animate({ 'opacity': '0' })

      $(this).children('#section2_img_text').children('h5').animate({ 'bottom': '200px', 'opacity': '0.3' }, 500)
      $(this).children('#section2_img_text').children('p').animate({ 'bottom': '150px', 'opacity': '0' }, 300)

      $(this).children('#line').children('div').eq(0).stop().animate({ 'right': '-480px' }, 400)
      $(this).children('#line').children('div').eq(1).stop().animate({ 'top': '-680px' }, 400)
      $(this).children('#line').children('div').eq(2).stop().animate({ 'left': '-480px' }, 400)
      $(this).children('#line').children('div').eq(3).stop().animate({ 'bottom': '-680px' }, 400)

      $(this).children('#line').children('div').eq(4).stop().animate({ 'right': '20px' }, 400)
      $(this).children('#line').children('div').eq(5).stop().animate({ 'top': '20px' }, 400)
      $(this).children('#line').children('div').eq(6).stop().animate({ 'left': '20px' }, 400)
      $(this).children('#line').children('div').eq(7).stop().animate({ 'bottom': '20px' }, 400)
    })

    //section3 슬라이드 클릭 
    section3_slide_num = 0;
    section3_slide_mv = 0





    //section3 슬라이드 오른쪽 클릭
    $('#slide_select_line img:nth-child(6)').click(function() {

      //크거나 같다>=
      if (section3_slide_num >= 11) { section3_slide_num = -1; }
      section3_slide_num = section3_slide_num + 1;


      if (section3_slide_mv <= -3000) {
        section3_slide_mv = 0;
        //-3000일 때 0이 되도록. 
        $('#slide_left_img_wrap').css('left', 0)
        $('#slide_right_img_wrap').css('left', 0)
        //초기값 css로 0 적어주기
      }
      section3_slide_mv = section3_slide_mv - 250;


      //왼쪽 슬라이드
      $('#slide_left_img_wrap').animate({ 'left': section3_slide_mv }, 600)

      //오른쪽 슬라이드
      $('#slide_right_img_wrap').animate({ 'left': section3_slide_mv }, 600)

      //select 
      $('#slide_select_img_wrap>div').eq(section3_slide_num).siblings().fadeOut(600, function() {
        $(this).children().children('h5').animate({ letterSpacing: 0 }, 500);
      })
      $('#slide_select_img_wrap>div').eq(section3_slide_num).fadeIn(600, function() {
        $(this).children().children('h5').animate({ letterSpacing: 3 }, 500);
      })

      if (section3_slide_num == 0) {
        $('#section3_text1 h2').html('Capricornus Constellation')
        $('#section3_text1 p').html('Facts About the Sea Goat')
        $('#section3_text2 p').html('Located in the Southern Hemisphere, Capricornus represents a creature that is a blend of fish and goat;</br>the name means "goat horn" in Latin.</br>In astrology, which is not a science, Capricorn is the 10th sign in the Zodiac and represents those born between Dec. 22 and Jan. 19.')
      } else if (section3_slide_num == 1) {
        $('#section3_text1 h2').html('Aquarius Constellation')
        $('#section3_text1 p').html('Facts About the Water Bearer')
        $('#section3_text2 p').html('Aquarius is a constellation of the Zodiac and one of the oldest documented constellations.</br>In astrology, which is not a science, Aquarius is the 11th sign in the Zodiac and represents those born between Jan. 20 and Feb. 18.')
      } else if (section3_slide_num == 2) {
        $('#section3_text1 h2').html('Pisces Constellation')
        $('#section3_text1 p').html('Facts About the Fishes')
        $('#section3_text2 p').html('Pisces, named for the Latin plural of fish, occupies 889 square degrees, making it the 14th largest constellation overall.</br>In astrology, which is not a science, Pisces is the 12th sign in the Zodiac and represents those born between Feb. 20 and March 20.')
      } else if (section3_slide_num == 3) {
        $('#section3_text1 h2').html('Aries Constellation')
        $('#section3_text1 p').html('Facts About the Ram')
        $('#section3_text2 p').html('Aries the Ram is a mid-size constellation, ranking 39th in size among the 88 modern-day constellations.</br>In astrology, which is not a science, Aries is the first sign of the Zodiac, marking the beginning of the astrological year and representing</br>those born between March 21 and April 19.')
      } else if (section3_slide_num == 4) {
        $('#section3_text1 h2').html('Taurus Constellation')
        $('#section3_text1 p').html('Facts About the Bull')
        $('#section3_text2 p').html('Taurus the Bull is hard to miss as he charges through the northern winter sky as it is one of the most prominent and visible of all of the constellations.</br>The Bull is also one of the oldest documented constellations, with details of the constellation going as far back as the Early Bronze Age.</br>Taurus is the second sign in the Zodiac and represents those born between April 20 and May 20. ')
      } else if (section3_slide_num == 5) {
        $('#section3_text1 h2').html('Gemini Constellation')
        $('#section3_text1 p').html('Facts About the Twins')
        $('#section3_text2 p').html(' Gemini is one of the Zodiac constellations and one of the 48 constellations described by the 2nd century astronomer Ptolemy.</br>Astrology is not a science, but Gemini is one of the 13 constellations of the Zodiac.</br>It is the third sign of the Zodiac and represents those born between May 20 and June 20 and is an air sign.')
      } else if (section3_slide_num == 6) {
        $('#section3_text1 h2').html('Cancer Constellation')
        $('#section3_text1 p').html('Facts About the Crab')
        $('#section3_text2 p').html('Cancer, which is Latin for crab, is the dimmest of the 13 constellations of the Zodiac, having only two stars above the fourth magnitude.</br>In astrology, which is not a science, Cancer is the fourth sign of the Zodiac and represents those born between June 20 and July 22.')
      } else if (section3_slide_num == 7) {
        $('#section3_text1 h2').html('Leo Constellation')
        $('#section3_text1 p').html('Facts About the Lion')
        $('#section3_text2 p').html('Leo the Lion is one of the earliest recognized constellations.</br>Astrology is not a science, but Leo is one of the 13 constellations of the Zodiac.</br>Leo is the fifth sign of the Zodiac and represents those born July 22 to August 22.')
      } else if (section3_slide_num == 8) {
        $('#section3_text1 h2').html('Virgo Constellation')
        $('#section3_text1 p').html('Facts about the Virgin')
        $('#section3_text2 p').html('Virgo is a congested constellation with dozens of known exoplanets and at least a dozen Messier objects.</br>It is the largest constellation of the Zodiac and the second-largest constellation overall, behind Hydra.</br>In astrology, which is not a science, Virgo is the fifth sign in the Zodiac and represents those born between Aug. 23 and Sept. 22.')
      } else if (section3_slide_num == 9) {
        $('#section3_text1 h2').html('Libra Constellation')
        $('#section3_text1 p').html('Facts About the Scales')
        $('#section3_text2 p').html('Libra is Latin for weighing scales, making it the only constellation of the Zodiac representing an inanimate object.</br>In astrology, which is not a science, Libra is the seventh sign in the Zodiac and represents those born between Sept. 23 and Oct. 22.')
      } else if (section3_slide_num == 10) {
        $('#section3_text1 h2').html('Scorpius Constellation')
        $('#section3_text1 p').html('Facts About the Scorpion')
        $('#section3_text2 p').html('The Scorpius constellation has intrigued people for centuries, not only for its distinctive shape, but also because it is one of the brightest constellations in the sky.</br>In astrology, which is not a science, the constellation is called Scorpio.</br>It is the eighth sign in the Zodiac and represents those born between Oct. 24 and Nov. 22.')
      } else if (section3_slide_num == 11) {
        $('#section3_text1 h2').html('Sagittarius Constellation')
        $('#section3_text1 p').html('Facts About the Archer')
        $('#section3_text2 p').html('Occupying 867 square degrees, Sagittarius is the largest constellation in the Southern Hemisphere and the 15th largest constellation overall.</br>In astrology, which is not a science, Sagittarius is the ninth sign in the Zodiac and represents those born between Nov. 22 and Dec. 21.')
      }






    })



    //section3 슬라이드 왼쪽 클릭
    $('#slide_select_line img:nth-child(5)').click(function() {

      if (section3_slide_num <= 0) { section3_slide_num = 12 }
      section3_slide_num = section3_slide_num - 1;

      if (section3_slide_mv >= 0) {
        section3_slide_mv = -3000;
        $('#slide_left_img_wrap').css('left', -3000)
        $('#slide_right_img_wrap').css('left', -3000)
      }

      section3_slide_mv = section3_slide_mv + 250;


      //왼쪽 슬라이드 
      $('#slide_left_img_wrap').animate({ 'left': section3_slide_mv }, 600)

      //오른쪽 슬라이드
      $('#slide_right_img_wrap').animate({ 'left': section3_slide_mv }, 600)




      //select 
      $('#slide_select_img_wrap>div').eq(section3_slide_num).siblings().fadeOut(600, function() {
        $(this).children().children('h5').animate({ letterSpacing: 0 }, 500);
      })
      $('#slide_select_img_wrap>div').eq(section3_slide_num).fadeIn(600, function() {
        $(this).children().children('h5').animate({ letterSpacing: 3 }, 500);
      })

      if (section3_slide_num == 0) {
        $('#section3_text1 h2').html('Capricornus Constellation')
        $('#section3_text1 p').html('Facts About the Sea Goat')
        $('#section3_text2 p').html('Located in the Southern Hemisphere, Capricornus represents a creature that is a blend of fish and goat;</br>the name means "goat horn" in Latin.</br>In astrology, which is not a science, Capricorn is the 10th sign in the Zodiac and represents those born between Dec. 22 and Jan. 19.')
      } else if (section3_slide_num == 1) {
        $('#section3_text1 h2').html('Aquarius Constellation')
        $('#section3_text1 p').html('Facts About the Water Bearer')
        $('#section3_text2 p').html('Aquarius is a constellation of the Zodiac and one of the oldest documented constellations.</br>In astrology, which is not a science, Aquarius is the 11th sign in the Zodiac and represents those born between Jan. 20 and Feb. 18.')
      } else if (section3_slide_num == 2) {
        $('#section3_text1 h2').html('Pisces Constellation')
        $('#section3_text1 p').html('Facts About the Fishes')
        $('#section3_text2 p').html('Pisces, named for the Latin plural of fish, occupies 889 square degrees, making it the 14th largest constellation overall.</br>In astrology, which is not a science, Pisces is the 12th sign in the Zodiac and represents those born between Feb. 20 and March 20.')
      } else if (section3_slide_num == 3) {
        $('#section3_text1 h2').html('Aries Constellation')
        $('#section3_text1 p').html('Facts About the Ram')
        $('#section3_text2 p').html('Aries the Ram is a mid-size constellation, ranking 39th in size among the 88 modern-day constellations.</br>In astrology, which is not a science, Aries is the first sign of the Zodiac, marking the beginning of the astrological year and representing</br>those born between March 21 and April 19.')
      } else if (section3_slide_num == 4) {
        $('#section3_text1 h2').html('Taurus Constellation')
        $('#section3_text1 p').html('Facts About the Bull')
        $('#section3_text2 p').html('Taurus the Bull is hard to miss as he charges through the northern winter sky as it is one of the most prominent and visible of all of the constellations.</br>The Bull is also one of the oldest documented constellations, with details of the constellation going as far back as the Early Bronze Age.</br>Taurus is the second sign in the Zodiac and represents those born between April 20 and May 20. ')
      } else if (section3_slide_num == 5) {
        $('#section3_text1 h2').html('Gemini Constellation')
        $('#section3_text1 p').html('Facts About the Twins')
        $('#section3_text2 p').html(' Gemini is one of the Zodiac constellations and one of the 48 constellations described by the 2nd century astronomer Ptolemy.</br>Astrology is not a science, but Gemini is one of the 13 constellations of the Zodiac.</br>It is the third sign of the Zodiac and represents those born between May 20 and June 20 and is an air sign.')
      } else if (section3_slide_num == 6) {
        $('#section3_text1 h2').html('Cancer Constellation')
        $('#section3_text1 p').html('Facts About the Crab')
        $('#section3_text2 p').html('Cancer, which is Latin for crab, is the dimmest of the 13 constellations of the Zodiac, having only two stars above the fourth magnitude.</br>In astrology, which is not a science, Cancer is the fourth sign of the Zodiac and represents those born between June 20 and July 22.')
      } else if (section3_slide_num == 7) {
        $('#section3_text1 h2').html('Leo Constellation')
        $('#section3_text1 p').html('Facts About the Lion')
        $('#section3_text2 p').html('Leo the Lion is one of the earliest recognized constellations.</br>Astrology is not a science, but Leo is one of the 13 constellations of the Zodiac.</br>Leo is the fifth sign of the Zodiac and represents those born July 22 to August 22.')
      } else if (section3_slide_num == 8) {
        $('#section3_text1 h2').html('Virgo Constellation')
        $('#section3_text1 p').html('Facts about the Virgin')
        $('#section3_text2 p').html('Virgo is a congested constellation with dozens of known exoplanets and at least a dozen Messier objects.</br>It is the largest constellation of the Zodiac and the second-largest constellation overall, behind Hydra.</br>In astrology, which is not a science, Virgo is the fifth sign in the Zodiac and represents those born between Aug. 23 and Sept. 22.')
      } else if (section3_slide_num == 9) {
        $('#section3_text1 h2').html('Libra Constellation')
        $('#section3_text1 p').html('Facts About the Scales')
        $('#section3_text2 p').html('Libra is Latin for weighing scales, making it the only constellation of the Zodiac representing an inanimate object.</br>In astrology, which is not a science, Libra is the seventh sign in the Zodiac and represents those born between Sept. 23 and Oct. 22.')
      } else if (section3_slide_num == 10) {
        $('#section3_text1 h2').html('Scorpius Constellation')
        $('#section3_text1 p').html('Facts About the Scorpion')
        $('#section3_text2 p').html('The Scorpius constellation has intrigued people for centuries, not only for its distinctive shape, but also because it is one of the brightest constellations in the sky.</br>In astrology, which is not a science, the constellation is called Scorpio.</br>It is the eighth sign in the Zodiac and represents those born between Oct. 24 and Nov. 22.')
      } else if (section3_slide_num == 11) {
        $('#section3_text1 h2').html('Sagittarius Constellation')
        $('#section3_text1 p').html('Facts About the Archer')
        $('#section3_text2 p').html('Occupying 867 square degrees, Sagittarius is the largest constellation in the Southern Hemisphere and the 15th largest constellation overall.</br>In astrology, which is not a science, Sagittarius is the ninth sign in the Zodiac and represents those born between Nov. 22 and Dec. 21.')
      }



    })


    //section3 별자리 light
    $('#slide_select').mouseenter(function() {
      $('#slide_select_light img:eq(0)').animate({ 'opacity': '1' }, 1000)
      $('#add_light div').animate({ 'opacity': '0.8' }, 1000)
    })
    $('#slide_select').mouseleave(function() {
      $('#slide_select_light img:eq(0)').animate({ 'opacity': '0' }, 1000)
      $('#add_light div').animate({ 'opacity': '0' }, 1000)
    })

    //section3 별자리
    $('#select1 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select1 div:nth-child(3)').delay(600).animate({ 'opacity': '0.8' }, 800)
      $('#select1 div:nth-child(3)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select1 div:nth-child(4)').delay(900).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select1 div:nth-child(4)').delay(600).animate({ 'opacity': '1' }, 700)
      $('#select1 div:nth-child(4)').animate({ 'opacity': '0' }, 700, aaa)
    })
    $('#select1 div:nth-child(5)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select1 div:nth-child(5)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select1 div:nth-child(5)').animate({ 'opacity': '0' }, 660, aaa)
    })
    $('#select1 div:nth-child(6)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select1 div:nth-child(6)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select1 div:nth-child(6)').animate({ 'opacity': '0' }, 740, aaa)
    })

    $('#select1 div:nth-child(7)').delay(400).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select1 div:nth-child(7)').delay(600).animate({ 'opacity': '1' }, 800)
      $('#select1 div:nth-child(7)').animate({ 'opacity': '0' }, 640, aaa)
    })
    $('#select1 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select1 div:nth-child(8)').delay(700).animate({ 'opacity': '1' }, 700)
      $('#select1 div:nth-child(8)').animate({ 'opacity': '0' }, 840, aaa)
    })

    $('#select1 div:nth-child(9)').delay(650).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select1 div:nth-child(9)').delay(300).animate({ 'opacity': '1' }, 700)
      $('#select1 div:nth-child(9)').animate({ 'opacity': '0' }, 720, aaa)
    })
    $('#select1 div:nth-child(10)').delay(500).animate({ 'opacity': '0' }, 600, function aaa() {
      $('#select1 div:nth-child(10)').delay(300).animate({ 'opacity': '1' }, 900)
      $('#select1 div:nth-child(10)').animate({ 'opacity': '0' }, 739, aaa)
    })


    //select2 별자리
    $('#selec2 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select2 div:nth-child(3)').delay(1200).animate({ 'opacity': '1' }, 650)
      $('#select2 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 800, aaa)
    })

    $('#select2 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select2 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 800)
      $('#select2 div:nth-child(4)').animate({ 'opacity': '0' }, 650, aaa)
    })
    $('#select2 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select2 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select2 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 570, aaa)
    })

    $('#select2 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select2 div:nth-child(6)').delay(600).animate({ 'opacity': '0.8' }, 800)
      $('#select2 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select2 div:nth-child(7)').delay(900).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select2 div:nth-child(7)').delay(600).animate({ 'opacity': '1' }, 700)
      $('#select2 div:nth-child(7)').animate({ 'opacity': '0' }, 800, aaa)
    })
    $('#select2 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select2 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select2 div:nth-child(8)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select2 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 600, function aaa() {
      $('#select2 div:nth-child(9)').delay(200).animate({ 'opacity': '1' }, 650)
      $('#select2 div:nth-child(9)').animate({ 'opacity': '0' }, 710, aaa)
    })

    $('#select2 div:nth-child(10)').delay(400).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select2 div:nth-child(10)').delay(600).animate({ 'opacity': '1' }, 800)
      $('#select2 div:nth-child(10)').animate({ 'opacity': '0' }, 670, aaa)
    })
    $('#select2 div:nth-child(11)').delay(500).animate({ 'opacity': '0' }, 650, function aaa() {
      $('#select2 div:nth-child(11)').delay(800).animate({ 'opacity': '1' }, 500)
      $('#select2 div:nth-child(11)').animate({ 'opacity': '0' }, 620, aaa)
    })
    $('#select2 div:nth-child(12)').delay(300).animate({ 'opacity': '0' }, 650, function aaa() {
      $('#select2 div:nth-child(12)').delay(450).animate({ 'opacity': '1' }, 600)
      $('#select2 div:nth-child(12)').animate({ 'opacity': '0' }, 690, aaa)
    })


    //select3 별자리
    $('#select3 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select3 div:nth-child(3)').delay(560).animate({ 'opacity': '1' }, 650)
      $('#select3 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select3 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select3 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 800)
      $('#select3 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select3 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select3 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select3 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select3 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select3 div:nth-child(6)').delay(600).animate({ 'opacity': '0.8' }, 800)
      $('#select3 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select3 div:nth-child(7)').delay(900).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select3 div:nth-child(7)').delay(600).animate({ 'opacity': '1' }, 700)
      $('#select3 div:nth-child(7)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select3 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select3 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select3 div:nth-child(8)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select3 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select3 div:nth-child(9)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select3 div:nth-child(9)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select3 div:nth-child(10)').delay(400).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select3 div:nth-child(10)').delay(600).animate({ 'opacity': '1' }, 800)
      $('#select3 div:nth-child(10)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select3 div:nth-child(11)').delay(500).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select3 div:nth-child(11)').delay(700).animate({ 'opacity': '1' }, 700)
      $('#select3 div:nth-child(11)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select3 div:nth-child(12)').delay(650).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select3 div:nth-child(12)').delay(300).animate({ 'opacity': '1' }, 700)
      $('#select3 div:nth-child(12)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select3 div:nth-child(13)').delay(500).animate({ 'opacity': '0' }, 600, function aaa() {
      $('#select3 div:nth-child(13)').delay(300).animate({ 'opacity': '1' }, 900)
      $('#select3 div:nth-child(13)').animate({ 'opacity': '0' }, 500, aaa)
    })


    //select4 별자리
    $('#select4 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select4 div:nth-child(3)').delay(600).animate({ 'opacity': '1' }, 650)
      $('#select4 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select4 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select4 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 800)
      $('#select4 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select4 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select4 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select4 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select4 div:nth-child(6)').delay(500).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select4 div:nth-child(6)').delay(600).animate({ 'opacity': '0.8' }, 800)
      $('#select4 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })


    //select5 별자리
    $('#select5 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select5 div:nth-child(3)').delay(560).animate({ 'opacity': '1' }, 650)
      $('#select5 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select5 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select5 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 500)
      $('#select5 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select5 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select5 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select5 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select5 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select5 div:nth-child(6)').delay(600).animate({ 'opacity': '0.8' }, 1000)
      $('#select5 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select5 div:nth-child(7)').delay(250).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select5 div:nth-child(7)').delay(550).animate({ 'opacity': '1' }, 700)
      $('#select5 div:nth-child(7)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select5 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select5 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select5 div:nth-child(8)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select5 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select5 div:nth-child(9)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select5 div:nth-child(9)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select5 div:nth-child(10)').delay(400).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select5 div:nth-child(10)').delay(600).animate({ 'opacity': '1' }, 800)
      $('#select5 div:nth-child(10)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select5 div:nth-child(11)').delay(500).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select5 div:nth-child(11)').delay(700).animate({ 'opacity': '1' }, 700)
      $('#select5 div:nth-child(11)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select5 div:nth-child(12)').delay(650).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select5 div:nth-child(12)').delay(300).animate({ 'opacity': '1' }, 600)
      $('#select5 div:nth-child(12)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select5 div:nth-child(13)').delay(500).animate({ 'opacity': '0' }, 600, function aaa() {
      $('#select5 div:nth-child(13)').delay(300).animate({ 'opacity': '1' }, 900)
      $('#select5 div:nth-child(13)').animate({ 'opacity': '0' }, 500, aaa)
    })

    //select6 별자리
    $('#select6 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select6 div:nth-child(3)').delay(560).animate({ 'opacity': '1' }, 650)
      $('#select6 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select6 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select6 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 500)
      $('#select6 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select6 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select6 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select6 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select6 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select6 div:nth-child(6)').delay(600).animate({ 'opacity': '0.8' }, 1000)
      $('#select6 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select6 div:nth-child(7)').delay(250).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select6 div:nth-child(7)').delay(550).animate({ 'opacity': '1' }, 700)
      $('#select6 div:nth-child(7)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select6 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select6 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select6 div:nth-child(8)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select6 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select6 div:nth-child(9)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select6 div:nth-child(9)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select6 div:nth-child(10)').delay(400).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select6 div:nth-child(10)').delay(600).animate({ 'opacity': '1' }, 800)
      $('#select6 div:nth-child(10)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select6 div:nth-child(11)').delay(500).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select6 div:nth-child(11)').delay(700).animate({ 'opacity': '1' }, 700)
      $('#select6 div:nth-child(11)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select6 div:nth-child(12)').delay(650).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select6 div:nth-child(12)').delay(300).animate({ 'opacity': '1' }, 600)
      $('#select6 div:nth-child(12)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select6 div:nth-child(13)').delay(500).animate({ 'opacity': '0' }, 600, function aaa() {
      $('#select6 div:nth-child(13)').delay(300).animate({ 'opacity': '1' }, 900)
      $('#select6 div:nth-child(13)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select6 div:nth-child(14)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select6 div:nth-child(14)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select6 div:nth-child(14)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select6 div:nth-child(15)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select6 div:nth-child(15)').delay(600).animate({ 'opacity': '0.8' }, 800)
      $('#select6 div:nth-child(15)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select6 div:nth-child(16)').delay(900).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select6 div:nth-child(16)').delay(600).animate({ 'opacity': '1' }, 700)
      $('#select6 div:nth-child(16)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select6 div:nth-child(17)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select6 div:nth-child(17)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select6 div:nth-child(17)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select6 div:nth-child(18)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select6 div:nth-child(18)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select6 div:nth-child(18)').animate({ 'opacity': '0' }, 500, aaa)
    })

    //select7 별자리
    $('#select7 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select7 div:nth-child(3)').delay(560).animate({ 'opacity': '1' }, 650)
      $('#select7 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select7 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select7 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 500)
      $('#select7 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select7 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select7 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select7 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select7 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select7 div:nth-child(6)').delay(600).animate({ 'opacity': '0.8' }, 1000)
      $('#select7 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select7 div:nth-child(7)').delay(250).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select7 div:nth-child(7)').delay(550).animate({ 'opacity': '1' }, 700)
      $('#select7 div:nth-child(7)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select7 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select7 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select7 div:nth-child(8)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select7 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select7 div:nth-child(9)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select7 div:nth-child(9)').animate({ 'opacity': '0' }, 500, aaa)
    })

    //select8 별자리
    $('#select8 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select8 div:nth-child(3)').delay(560).animate({ 'opacity': '1' }, 650)
      $('#select8 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select8 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select8 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 500)
      $('#select8 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select8 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select8 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select8 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select8 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select8 div:nth-child(6)').delay(600).animate({ 'opacity': '0.8' }, 1000)
      $('#select8 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select8 div:nth-child(7)').delay(250).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select8 div:nth-child(7)').delay(550).animate({ 'opacity': '1' }, 700)
      $('#select8 div:nth-child(7)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select8 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select8 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select8 div:nth-child(8)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select8 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select8 div:nth-child(9)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select8 div:nth-child(9)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select8 div:nth-child(10)').delay(400).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select8 div:nth-child(10)').delay(600).animate({ 'opacity': '1' }, 800)
      $('#select8 div:nth-child(10)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select8 div:nth-child(11)').delay(500).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select8 div:nth-child(11)').delay(700).animate({ 'opacity': '1' }, 700)
      $('#select8 div:nth-child(11)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select8 div:nth-child(12)').delay(650).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select8 div:nth-child(12)').delay(300).animate({ 'opacity': '1' }, 600)
      $('#select8 div:nth-child(12)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select8 div:nth-child(13)').delay(500).animate({ 'opacity': '0' }, 600, function aaa() {
      $('#select8 div:nth-child(13)').delay(300).animate({ 'opacity': '1' }, 900)
      $('#select8 div:nth-child(13)').animate({ 'opacity': '0' }, 500, aaa)
    })



    //select9 별자리
    $('#select9 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select9 div:nth-child(3)').delay(560).animate({ 'opacity': '1' }, 650)
      $('#select9 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select9 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select9 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 500)
      $('#select9 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select9 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select9 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select9 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select9 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 400, function aaa() {
      $('#select9 div:nth-child(6)').delay(500).animate({ 'opacity': '0.8' }, 700)
      $('#select9 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select9 div:nth-child(7)').delay(250).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select9 div:nth-child(7)').delay(550).animate({ 'opacity': '1' }, 700)
      $('#select9 div:nth-child(7)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select9 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select9 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select9 div:nth-child(8)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select9 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select9 div:nth-child(9)').delay(450).animate({ 'opacity': '1' }, 400)
      $('#select9 div:nth-child(9)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select9 div:nth-child(10)').delay(400).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select9 div:nth-child(10)').delay(600).animate({ 'opacity': '1' }, 800)
      $('#select9 div:nth-child(10)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select9 div:nth-child(11)').delay(500).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select9 div:nth-child(11)').delay(700).animate({ 'opacity': '1' }, 700)
      $('#select9 div:nth-child(11)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select9 div:nth-child(12)').delay(450).animate({ 'opacity': '0' }, 450, function aaa() {
      $('#select9 div:nth-child(12)').delay(300).animate({ 'opacity': '1' }, 600)
      $('#select9 div:nth-child(12)').animate({ 'opacity': '0' }, 600, aaa)
    })
    $('#select9 div:nth-child(13)').delay(500).animate({ 'opacity': '0' }, 600, function aaa() {
      $('#select9 div:nth-child(13)').delay(300).animate({ 'opacity': '1' }, 900)
      $('#select9 div:nth-child(13)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select9 div:nth-child(14)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select9 div:nth-child(14)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select9 div:nth-child(14)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select9 div:nth-child(15)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select9 div:nth-child(15)').delay(600).animate({ 'opacity': '0.8' }, 800)
      $('#select9 div:nth-child(15)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select9 div:nth-child(16)').delay(900).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select9 div:nth-child(16)').delay(600).animate({ 'opacity': '1' }, 700)
      $('#select9 div:nth-child(16)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select9 div:nth-child(17)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select9 div:nth-child(17)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select9 div:nth-child(17)').animate({ 'opacity': '0' }, 500, aaa)
    })


    //select9 별자리
    $('#select10 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select10 div:nth-child(3)').delay(560).animate({ 'opacity': '1' }, 650)
      $('#select10 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select10 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select10 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 500)
      $('#select10 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select10 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select10 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select10 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select10 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 400, function aaa() {
      $('#select10 div:nth-child(6)').delay(500).animate({ 'opacity': '0.8' }, 700)
      $('#select10 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select10 div:nth-child(7)').delay(250).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select10 div:nth-child(7)').delay(550).animate({ 'opacity': '1' }, 700)
      $('#select10 div:nth-child(7)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select10 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select10 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select10 div:nth-child(8)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select10 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select10 div:nth-child(9)').delay(450).animate({ 'opacity': '1' }, 400)
      $('#select10 div:nth-child(9)').animate({ 'opacity': '0' }, 500, aaa)
    })

    //select11 별자리
    $('#select11 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select11 div:nth-child(3)').delay(560).animate({ 'opacity': '1' }, 650)
      $('#select11 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select11 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select11 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 500)
      $('#select11 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select11 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select11 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select11 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select11 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select11 div:nth-child(6)').delay(600).animate({ 'opacity': '0.8' }, 1000)
      $('#select11 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select11 div:nth-child(7)').delay(250).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select11 div:nth-child(7)').delay(550).animate({ 'opacity': '1' }, 700)
      $('#select11 div:nth-child(7)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select11 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select11 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select11 div:nth-child(8)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select11 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select11 div:nth-child(9)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select11 div:nth-child(9)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select11 div:nth-child(10)').delay(400).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select11 div:nth-child(10)').delay(600).animate({ 'opacity': '1' }, 800)
      $('#select11 div:nth-child(10)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select11 div:nth-child(11)').delay(500).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select11 div:nth-child(11)').delay(700).animate({ 'opacity': '1' }, 700)
      $('#select11 div:nth-child(11)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select11 div:nth-child(12)').delay(650).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select11 div:nth-child(12)').delay(300).animate({ 'opacity': '1' }, 600)
      $('#select11 div:nth-child(12)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select11 div:nth-child(13)').delay(500).animate({ 'opacity': '0' }, 600, function aaa() {
      $('#select11 div:nth-child(13)').delay(300).animate({ 'opacity': '1' }, 900)
      $('#select11 div:nth-child(13)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select11 div:nth-child(14)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select11 div:nth-child(14)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select11 div:nth-child(14)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select11 div:nth-child(15)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select11 div:nth-child(15)').delay(600).animate({ 'opacity': '0.8' }, 800)
      $('#select11 div:nth-child(15)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select11 div:nth-child(16)').delay(900).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select11 div:nth-child(16)').delay(600).animate({ 'opacity': '1' }, 700)
      $('#select11 div:nth-child(16)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select11 div:nth-child(17)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select11 div:nth-child(17)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select11 div:nth-child(17)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select11 div:nth-child(18)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select11 div:nth-child(18)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select11 div:nth-child(18)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select11 div:nth-child(19)').delay(650).animate({ 'opacity': '0' }, 530, function aaa() {
      $('#select11 div:nth-child(19)').delay(300).animate({ 'opacity': '1' }, 670)
      $('#select11 div:nth-child(19)').animate({ 'opacity': '0' }, 500, aaa)
    })

    //select12 별자리
    $('#select12 div:nth-child(3)').animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select12 div:nth-child(3)').delay(560).animate({ 'opacity': '1' }, 650)
      $('#select12 div:nth-child(3)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select12 div:nth-child(4)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select12 div:nth-child(4)').delay(700).animate({ 'opacity': '1' }, 500)
      $('#select12 div:nth-child(4)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(5)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select12 div:nth-child(5)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select12 div:nth-child(5)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select12 div:nth-child(6)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select12 div:nth-child(6)').delay(600).animate({ 'opacity': '0.8' }, 1000)
      $('#select12 div:nth-child(6)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select12 div:nth-child(7)').delay(250).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select12 div:nth-child(7)').delay(550).animate({ 'opacity': '1' }, 700)
      $('#select12 div:nth-child(7)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(8)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select12 div:nth-child(8)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select12 div:nth-child(8)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(9)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select12 div:nth-child(9)').delay(200).animate({ 'opacity': '1' }, 700)
      $('#select12 div:nth-child(9)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select12 div:nth-child(10)').delay(400).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select12 div:nth-child(10)').delay(600).animate({ 'opacity': '1' }, 800)
      $('#select12 div:nth-child(10)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(11)').delay(500).animate({ 'opacity': '0' }, 850, function aaa() {
      $('#select12 div:nth-child(11)').delay(700).animate({ 'opacity': '1' }, 700)
      $('#select12 div:nth-child(11)').animate({ 'opacity': '0' }, 500, aaa)
    })

    $('#select12 div:nth-child(12)').delay(650).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select12 div:nth-child(12)').delay(300).animate({ 'opacity': '1' }, 600)
      $('#select12 div:nth-child(12)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(13)').delay(500).animate({ 'opacity': '0' }, 600, function aaa() {
      $('#select12 div:nth-child(13)').delay(300).animate({ 'opacity': '1' }, 900)
      $('#select12 div:nth-child(13)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(14)').animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select12 div:nth-child(14)').delay(500).animate({ 'opacity': '1' }, 550)
      $('#select12 div:nth-child(14)').delay(300).animate({ 'opacity': '0' }, 700, aaa)
    })

    $('#select12 div:nth-child(15)').delay(300).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select12 div:nth-child(15)').delay(600).animate({ 'opacity': '0.8' }, 800)
      $('#select12 div:nth-child(15)').animate({ 'opacity': '0' }, 750, aaa)
    })
    $('#select12 div:nth-child(16)').delay(900).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select12 div:nth-child(16)').delay(600).animate({ 'opacity': '1' }, 700)
      $('#select12 div:nth-child(16)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(17)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select12 div:nth-child(17)').delay(300).animate({ 'opacity': '1' }, 800)
      $('#select12 div:nth-child(17)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(18)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select12 div:nth-child(18)').delay(320).animate({ 'opacity': '1' }, 760)
      $('#select12 div:nth-child(18)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(19)').delay(650).animate({ 'opacity': '0' }, 530, function aaa() {
      $('#select12 div:nth-child(19)').delay(470).animate({ 'opacity': '1' }, 590)
      $('#select12 div:nth-child(19)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(20)').delay(900).animate({ 'opacity': '0' }, 500, function aaa() {
      $('#select12 div:nth-child(20)').delay(650).animate({ 'opacity': '1' }, 630)
      $('#select12 div:nth-child(20)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(21)').delay(500).animate({ 'opacity': '0' }, 550, function aaa() {
      $('#select12 div:nth-child(21)').delay(430).animate({ 'opacity': '1' }, 750)
      $('#select12 div:nth-child(21)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(22)').delay(500).animate({ 'opacity': '0' }, 700, function aaa() {
      $('#select12 div:nth-child(22)').delay(360).animate({ 'opacity': '1' }, 400)
      $('#select12 div:nth-child(22)').animate({ 'opacity': '0' }, 500, aaa)
    })
    $('#select12 div:nth-child(23)').delay(650).animate({ 'opacity': '0' }, 530, function aaa() {
      $('#select12 div:nth-child(23)').delay(450).animate({ 'opacity': '1' }, 670)
      $('#select12 div:nth-child(23)').animate({ 'opacity': '0' }, 500, aaa)
    })





    //section4

    $('#section4_first_wrap>div').mouseenter(function() {
      $(this).children('img').stop().animate({ 'opacity': '0.5' }, 500)
      $(this).siblings().children('img').stop().animate({ 'opacity': '0' }, 500)
    })
    $('#section4_first_wrap>div').mouseleave(function() {
      $(this).children('img').stop().animate({ 'opacity': '0' }, 500)

    })

    $('#section4_first_text1').click(function() {
      $('#section4_bl_bg').show()
      $('#section4_bl_bg').animate({ 'opacity': '0.5' }, 1000)
      $(this).fadeOut(400);
      $(this).siblings().fadeOut(400);
      //$(this).siblings().animate({'opacity':'0'},500)
      //$(this).animate({'opacity':'0'},500)]
      $('#section4_second1_wrap').fadeIn();

      $('#section4_second1_left').delay(400).animate({ 'opacity': '1', 'left': '0px' }, 1100)
      $('#section4_second1_left div').delay(1000).animate({ 'opacity': '1' }, 600, function() {
        $('#section4_second1_right_1 img').animate({ 'opacity': '1', 'top': '110px' }, 500)
        $('#section4_second1_right_1 div:nth-child(2)').delay(200).animate({ 'opacity': '1', 'top': '370px' }, 500)
        $('#section4_second1_right_2 img').delay(400).animate({ 'opacity': '1', 'top': '250px' }, 500)
        $('#section4_second1_right_2 div:nth-child(2)').delay(500).animate({ 'opacity': '1', 'top': '120px' }, 500)
      })
      $('#menu').hide()
      $('#section4_header').fadeOut();

      $('#arrow_wrap').animate({ 'opacity': '0' }, 500)
    })
    //second2
    $('#section4_first_text2').click(function() {
      $('#section4_bl_bg').show()
      $('#section4_bl_bg').animate({ 'opacity': '0.5' }, 1000)
      $(this).fadeOut(400);
      $(this).siblings().fadeOut(400);
      $('#section4_second2_wrap').fadeIn();
      $('#section4_second2_left').delay(400).animate({ 'opacity': '1', 'left': '0px' }, 1100)
      $('#section4_second2_left div').delay(1000).animate({ 'opacity': '1' }, 600, function() {
        $('#section4_second2_right_1 div:nth-child(2)').animate({ 'opacity': '1', 'top': '110px' }, 500)
        $('#section4_second2_right_1 img').delay(200).animate({ 'opacity': '1', 'top': '430px' }, 500)
        $('#section4_second2_right_2 img').delay(400).animate({ 'opacity': '1', 'top': '110px' }, 500)
        $('#section4_second2_right_2 div:nth-child(2)').delay(500).animate({ 'opacity': '1', 'top': '400px' }, 500)
      })
      $('#menu').hide()
      $('#section4_header').fadeOut();

      $('#arrow_wrap').animate({ 'opacity': '0' }, 500)
    })

    $('#section4_first_text3').click(function() {
      $('#section4_bl_bg').show()
      $('#section4_bl_bg').animate({ 'opacity': '0.5' }, 1000)
      $(this).fadeOut(400);
      $(this).siblings().fadeOut(400);
      $('#section4_second3_wrap').fadeIn();
      $('#section4_second3_left').delay(400).animate({ 'opacity': '1', 'left': '0px' }, 1100)
      $('#section4_second3_left div').delay(1000).animate({ 'opacity': '1' }, 600, function() {
        $('#section4_second3_right_1 img').animate({ 'opacity': '1', 'top': '110px' }, 500)
        $('#section4_second3_right_1 div:nth-child(2)').delay(200).animate({ 'opacity': '1', 'top': '370px' }, 500)
        $('#section4_second3_right_2 img').delay(400).animate({ 'opacity': '1', 'top': '250px' }, 500)
        $('#section4_second3_right_2 div:nth-child(2)').delay(500).animate({ 'opacity': '1', 'top': '120px' }, 500)
      })
      $('#menu').hide()
      $('#section4_header').fadeOut();

      $('#arrow_wrap').animate({ 'opacity': '0' }, 500)
    })

    $('#section4_first_text4').click(function() {
      $('#section4_bl_bg').show()
      $('#section4_bl_bg').animate({ 'opacity': '0.5' }, 1000)
      $(this).fadeOut(400);
      $(this).siblings().fadeOut(400);
      $('#section4_second4_wrap').fadeIn();
      $('#section4_second4_left').delay(400).animate({ 'opacity': '1', 'left': '0px' }, 1100)
      $('#section4_second4_left div').delay(1000).animate({ 'opacity': '1' }, 600, function() {
        $('#section4_second4_right_1 div:nth-child(2)').animate({ 'opacity': '1', 'top': '90px' }, 500)
        $('#section4_second4_right_1 img').delay(200).animate({ 'opacity': '1', 'top': '450px' }, 500)
        $('#section4_second4_right_2 img').delay(400).animate({ 'opacity': '1', 'top': '60px' }, 500)
        $('#section4_second4_right_2 div:nth-child(2)').delay(500).animate({ 'opacity': '1', 'top': '380px' }, 500)
      })
      $('#menu').hide()
      $('#section4_header').fadeOut();

      $('#arrow_wrap').animate({ 'opacity': '0' }, 500)
    })


    $('.section4_second_close_bt').click(function() {
      $('#section4_bl_bg').animate({ 'opacity': '0' }, 500)

      //section4 pop초기화

      $('#section4_second1_wrap').fadeOut(500)
      $('#section4_second2_wrap').fadeOut(500)
      $('#section4_second3_wrap').fadeOut(500)
      $('#section4_second4_wrap').fadeOut(500)

      //second1 초기화
      $('#section4_second1_left').stop()
      $('#section4_second1_left div').stop()
      $('#section4_second1_right_1 img').stop()
      $('#section4_second1_right_1 div:nth-child(2)').stop()
      $('#section4_second1_right_2 img').stop()
      $('#section4_second1_right_2 div:nth-child(2)').stop()

      $('#section4_second1_left').css({ 'opacity': '0', 'left': '80px' })
      $('#section4_second1_left div').css({ 'opacity': '0' })
      $('#section4_second1_right_1 img').css({ 'opacity': '0', 'top': '120px' })
      $('#section4_second1_right_1 div:nth-child(2)').css({ 'opacity': '0', 'top': '380px' })
      $('#section4_second1_right_2 img').css({ 'opacity': '0', 'top': '260px' })
      $('#section4_second1_right_2 div:nth-child(2)').css({ 'opacity': '0', 'top': '130px' })

      //second2 초기화
      $('#section4_second2_left').stop()
      $('#section4_second2_left div').stop()
      $('#section4_second2_right_1 img').stop()
      $('#section4_second2_right_1 div:nth-child(2)').stop()
      $('#section4_second2_right_2 img').stop()
      $('#section4_second2_right_2 div:nth-child(2)').stop()

      $('#section4_second2_left').css({ 'opacity': '0', 'left': '80px' })
      $('#section4_second2_left div').css({ 'opacity': '0' })
      $('#section4_second2_right_1 img').css({ 'opacity': '0', 'top': '440px' })
      $('#section4_second2_right_1 div:nth-child(2)').css({ 'opacity': '0', 'top': '120px' })
      $('#section4_second2_right_2 img').css({ 'opacity': '0', 'top': '120px' })
      $('#section4_second2_right_2 div:nth-child(2)').css({ 'opacity': '0', 'top': '410px' })

      //second3 초기화
      $('#section4_second3_left').stop()
      $('#section4_second3_left div').stop()
      $('#section4_second3_right_1 img').stop()
      $('#section4_second3_right_1 div:nth-child(2)').stop()
      $('#section4_second3_right_2 img').stop()
      $('#section4_second3_right_2 div:nth-child(2)').stop()

      $('#section4_second3_left').css({ 'opacity': '0', 'left': '80px' })
      $('#section4_second3_left div').css({ 'opacity': '0' })
      $('#section4_second3_right_1 img').css({ 'opacity': '0', 'top': '120px' })
      $('#section4_second3_right_1 div:nth-child(2)').css({ 'opacity': '0', 'top': '380px' })
      $('#section4_second3_right_2 img').css({ 'opacity': '0', 'top': '260px' })
      $('#section4_second3_right_2 div:nth-child(2)').css({ 'opacity': '0', 'top': '130px' })

      //second4 초기화
      $('#section4_second4_left').stop()
      $('#section4_second4_left div').stop()
      $('#section4_second4_right_1 img').stop()
      $('#section4_second4_right_1 div:nth-child(2)').stop()
      $('#section4_second4_right_2 img').stop()
      $('#section4_second4_right_2 div:nth-child(2)').stop()

      $('#section4_second4_left').css({ 'opacity': '0', 'left': '80px' })
      $('#section4_second4_left div').css({ 'opacity': '0' })
      $('#section4_second4_right_1 img').css({ 'opacity': '0', 'top': '460px' })
      $('#section4_second4_right_1 div:nth-child(2)').css({ 'opacity': '0', 'top': '100px' })
      $('#section4_second4_right_2 img').css({ 'opacity': '0', 'top': '70px' })
      $('#section4_second4_right_2 div:nth-child(2)').css({ 'opacity': '0', 'top': '390px' })



      $('#menu').fadeIn()

      $('#section4_first_wrap>div').fadeIn()
      $('#section4_header').fadeIn();

      $('#arrow_wrap').animate({ 'opacity': '1' }, 500)


    })



    //section5

    $("#section5_wrap").mCustomScrollbar({
      axis: "y",
      theme: "thin",
      autoExpandScrollbar: true,
      advanced: { autoExpandHorizontalScroll: true },
      mouseWheel: { scrollAmount: 600 },
      //                    원래 600


      //snapAmount:188,
      //snapOffset:65
    });

    setInterval(function() {
      s5_top = $('#section5_header').offset().top;
      if (s5_top < -100) {
        $('#section5_header #mouse').stop().animate({ opacity: 0 }, 80)
      }
      if (s5_top < -300) {
        $('#section5_1_text h4').delay(300).animate({ 'letter-spacing': '1px' }, 800)
      }
      if (s5_top < -800) {
        $('#section5_2_text h4').delay(300).animate({ 'letter-spacing': '1px' }, 800)
      }
      if (s5_top < -1300) {
        $('#section5_3_text h4').delay(300).animate({ 'letter-spacing': '1px' }, 800)
      }
      if (s5_top < -1800) {
        $('#section5_4_text h4').delay(300).animate({ 'letter-spacing': '1px' }, 800)
      } else if (s5_top > -100) {
        $('#section5_header #mouse').stop().animate({ opacity: 1 }, 80)
      }


    }, 30)


    $('#section5_header #mouse div:eq(1)').animate({ 'top': '38px', 'opacity': '0.2' }, 1000, function loopa() {
      $('#section5_header #mouse div:eq(1)').css({ 'top': '17px', 'opacity': '1' })
      $('#section5_header #mouse div:eq(1)').animate({ 'top': '38px', 'opacity': '0.2' }, 1000, loopa)
    })




    //section6 



    $('#section6_poly>div:eq(1)').children('div').delay(200).animate({ padding: 0 }, 0, function loopaa() {
      $('#section6_poly>div:eq(1)').children('div').animate({ 'opacity': '0.5' }, 1200, function() {
        $('#section6_poly>div:eq(1)').children('div').animate({ 'opacity': '0.1' }, 1200, loopaa)
      });
    });
    $('#section6_poly>div:eq(4)').children('div').delay(700).animate({ padding: 0 }, 0, function loopaa() {
      $('#section6_poly>div:eq(4)').children('div').animate({ 'opacity': '0.5' }, 1200, function() {
        $('#section6_poly>div:eq(4)').children('div').animate({ 'opacity': '0.1' }, 1200, loopaa)
      });
    });
    //---------------------------------------------------------------
    $('#section6_poly>div:eq(1)').mouseenter(function() {
      $(this).children('div').stop().animate({ 'opacity': '0' }, 400)
    })

    $('#section6_poly>div:eq(1)').mouseleave(function() {
      $(this).children('div').stop().animate({ 'opacity': '0.5' }, 400, function() {
        $('#section6_poly>div:eq(1)').children('div').animate({ padding: 0 }, 0, function loopaa() {
          $('#section6_poly>div:eq(1)').children('div').animate({ 'opacity': '0.5' }, 1200, function() {
            $('#section6_poly>div:eq(1)').children('div').animate({ 'opacity': '0.1' }, 1200, loopaa)
          });
        });
      })
    })
    //---------------------------------------------------------------


    $('#section6_poly>div:eq(4)').mouseenter(function() {
      $(this).children('div').stop().animate({ 'opacity': '0.1' }, 400)
    })

    $('#section6_poly>div:eq(4)').mouseleave(function() {
      $(this).children('div').stop().animate({ 'opacity': '0.5' }, 400, function() {
        $('#section6_poly>div:eq(4)').children('div').animate({ padding: 0 }, 0, function loopaa() {
          $('#section6_poly>div:eq(4)').children('div').animate({ 'opacity': '0.5' }, 1200, function() {
            $('#section6_poly>div:eq(4)').children('div').animate({ 'opacity': '0.1' }, 1200, loopaa)
          });
        });
      })
    })


    $('#section6_poly>div:nth-child(2)').click(function() {
      $('#section6_wrap').fadeOut(400)
      $('#section6_popbg').fadeIn(450)
    })

    $('#section6_pop_close').click(function() {
      $('#section6_wrap').fadeIn(400)
      $('#section6_popbg').fadeOut(400)
    })

    // section6_vedeo 슬라이드 오른쪽버튼 눌렀을 때
    section6_video = 0;
    $('#section6_pop_video ul li:nth-child(2)').click(function() {
      section6_video = section6_video - 495
      if (section6_video == -1980) {
        $('#section6_video_wrap').css({ 'left': '0' });
        section6_video = -495;
      }
      if (section6_video == -495) {
        $('#right_play_bt2').css({ 'background-color': 'rgba(31,47,62,0)' })
        $('#pop_right_select2').css({ 'opacity': '0.15' });

        $('#right_play_bt1').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select1').css({ 'opacity': '0' });
        $('#right_play_bt3').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select3').css({ 'opacity': '0' });

        $('#section6_pop_video p').html('<a href="https://www.youtube.com/watch?v=Vdb9IndsSXk&t=0s" target="_blank"><img src="6page/play_bt.png" alt="" /></a>')
      } else if (section6_video == -990) {
        $('#right_play_bt3').css({ 'background-color': 'rgba(31,47,62,0)' })
        $('#pop_right_select3').css({ 'opacity': '0.15' });

        $('#right_play_bt1').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select1').css({ 'opacity': '0' });
        $('#right_play_bt2').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select2').css({ 'opacity': '0' });

        $('#section6_pop_video p').html('<a href="https://www.youtube.com/watch?v=_vhf0RZg0fg" target="_blank"><img src="6page/play_bt.png" alt="" /></a>')
      } else if (section6_video == -1485) {
        $('#right_play_bt1').css({ 'background-color': 'rgba(31,47,62,0)' })
        $('#pop_right_select1').css({ 'opacity': '0.15' });

        $('#right_play_bt2').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select2').css({ 'opacity': '0' });
        $('#right_play_bt3').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select3').css({ 'opacity': '0' });

        $('#section6_pop_video p').html('<a href="https://www.youtube.com/watch?v=izYiDDt6d8s&t=0s" target="_blank"><img src="6page/play_bt.png" alt="" /></a>')
      }
      $('#section6_video_wrap').stop().animate({ left: section6_video }, 700);
    });

    // section6_vedeo 슬라이드 왼쪽버튼 눌렀을 때
    $('#section6_pop_video ul li:nth-child(1)').click(function() {
      section6_video = section6_video + 495
      if (section6_video == 495) { $('#section6_video_wrap').css({ 'left': '-1485px' });
        section6_video = -990; }

      if (section6_video == -990) {
        $('#right_play_bt3').css({ 'background-color': 'rgba(31,47,62,0)' })
        $('#pop_right_select3').css({ 'opacity': '0.15' });

        $('#right_play_bt1').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select1').css({ 'opacity': '0' });
        $('#right_play_bt2').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select2').css({ 'opacity': '0' });

        $('#section6_pop_video p').html('<a href="https://www.youtube.com/watch?v=_vhf0RZg0fg" target="_blank"><img src="6page/play_bt.png" alt="" /></a>')
      } else if (section6_video == -495) {
        $('#right_play_bt2').css({ 'background-color': 'rgba(31,47,62,0)' })
        $('#pop_right_select2').css({ 'opacity': '0.15' });

        $('#right_play_bt1').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select1').css({ 'opacity': '0' });
        $('#right_play_bt3').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select3').css({ 'opacity': '0' });

        $('#section6_pop_video p').html('<a href="https://www.youtube.com/watch?v=Vdb9IndsSXk&t=0s" target="_blank"><img src="6page/play_bt.png" alt="" /></a>')
      } else if (section6_video == 0) {
        $('#right_play_bt1').css({ 'background-color': 'rgba(31,47,62,0)' })
        $('#pop_right_select1').css({ 'opacity': '0.15' });

        $('#right_play_bt2').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select2').css({ 'opacity': '0' });
        $('#right_play_bt3').css({ 'background-color': 'rgba(31,47,62,0.7)' })
        $('#pop_right_select3').css({ 'opacity': '0' });

        $('#section6_pop_video p').html('<a href="https://www.youtube.com/watch?v=izYiDDt6d8s&t=0s" target="_blank"><img src="6page/play_bt.png" alt="" /></a>')
      }
      $('#section6_video_wrap').stop().animate({ left: section6_video }, 700);
    });

    $('#section6_pop_right>ul li').click(function() {
      video_num = $(this).index() * -495
      $('#section6_video_wrap').stop().animate({ left: video_num }, 700)

      section6_video = video_num

    })
    // section6_vedeo 슬라이드 오른쪽 메뉴 눌렀을 때
    $('#section6_pop_right>ul li:nth-child(1)').click(function() {
      $('#right_play_bt1').css({ 'background-color': 'rgba(31,47,62,0)' })
      $('#pop_right_select1').css({ 'opacity': '0.15' });
      //                $('#section6_video_wrap').stop().animate({'left':'0px'},700)


      $('#right_play_bt2').css({ 'background-color': 'rgba(31,47,62,0.7)' })
      $('#pop_right_select2').css({ 'opacity': '0' });
      $('#right_play_bt3').css({ 'background-color': 'rgba(31,47,62,0.7)' })
      $('#pop_right_select3').css({ 'opacity': '0' });

    })
    $('#section6_pop_right>ul li:nth-child(2)').click(function() {
      $('#right_play_bt2').css({ 'background-color': 'rgba(31,47,62,0)' })
      $('#pop_right_select2').css({ 'opacity': '0.15' });
      //                $('#section6_video_wrap').stop().animate({'left':'-495px'},700)

      $('#right_play_bt1').css({ 'background-color': 'rgba(31,47,62,0.7)' })
      $('#pop_right_select1').css({ 'opacity': '0' });
      $('#right_play_bt3').css({ 'background-color': 'rgba(31,47,62,0.7)' })
      $('#pop_right_select3').css({ 'opacity': '0' });

    })
    $('#section6_pop_right>ul li:nth-child(3)').click(function() {
      $('#right_play_bt3').css({ 'background-color': 'rgba(31,47,62,0)' })
      $('#pop_right_select3').css({ 'opacity': '0.15' });
      //                $('#section6_video_wrap').stop().animate({'left':'-990px'},700)

      $('#right_play_bt1').css({ 'background-color': 'rgba(31,47,62,0.7)' })
      $('#pop_right_select1').css({ 'opacity': '0' });
      $('#right_play_bt2').css({ 'background-color': 'rgba(31,47,62,0.7)' })
      $('#pop_right_select2').css({ 'opacity': '0' });

    })

    $('#section6_poly>div:nth-child(5)').click(function() {
      $('#section6_wrap').fadeOut(400)
      $('#section6_popbg_2').fadeIn(450)
    })

    $('#section6_pop_close_2').click(function() {
      $('#section6_wrap').fadeIn(400)
      $('#section6_popbg_2').fadeOut(400)
    })

    $(' #section6_pop_right_2 table tr td').click(function() {
      sumnail = $(this).children('img').attr('src')
      $('#section6_pop_left_2 div').hide().fadeIn().css({ 'background': 'url(' + sumnail + ')', 'background-size': 'cover' })
      $(this).children('div').fadeOut()
      $(this).siblings().children('div').fadeIn()
      $(this).parents('tr').siblings('tr').children('td').children('div').fadeIn()
    })


    $('#section6_poly>div:nth-child(5)').click(function() {
      $('#section6_wrap').fadeOut(400)
      $('#section6_popbg_2').fadeIn(450)
    })

    $('#section6_pop_close').click(function() {
      $('#section6_wrap').fadeIn(400)
      $('#section6_popbg_2').fadeOut(400)
    })





    //section7

    $('#spot1 div:nth-child(2)').animate({ padding: 0 }, 0, function loop() {
      $('#spot1 div:nth-child(2)').animate({ width: 12, height: 12, opacity: 0.1 }, 1000, function() {
        $('#spot1 div:nth-child(2)').animate({ width: 14, height: 14, opacity: 0.2 }, 1000, loop)
      });
    });

    $('#spot2 div:nth-child(2)').delay(300).animate({ padding: 0 }, 0, function loop() {
      $('#spot2 div:nth-child(2)').animate({ width: 12, height: 12, opacity: 0.1 }, 1000, function() {
        $('#spot2 div:nth-child(2)').animate({ width: 14, height: 14, opacity: 0.2 }, 1000, loop)
      });
    });

    $('#spot3 div:nth-child(2)').delay(600).animate({ padding: 0 }, 0, function loop() {
      $('#spot3 div:nth-child(2)').animate({ width: 12, height: 12, opacity: 0.1 }, 1000, function() {
        $('#spot3 div:nth-child(2)').animate({ width: 14, height: 14, opacity: 0.2 }, 1000, loop)
      });
    });

    $('#spot4 div:nth-child(2)').delay(900).animate({ padding: 0 }, 0, function loop() {
      $('#spot4 div:nth-child(2)').animate({ width: 12, height: 12, opacity: 0.1 }, 1000, function() {
        $('#spot4 div:nth-child(2)').animate({ width: 14, height: 14, opacity: 0.2 }, 1000, loop)
      });
    });
    //---------------------------------
    $('#spot1 div:nth-child(1)').animate({ padding: 0 }, 0, function loop() {
      $('#spot1 div:nth-child(1)').animate({ opacity: 0.1 }, 700, function() {
        $('#spot1 div:nth-child(1)').animate({ opacity: 0.8 }, 700, loop)
      });
    });

    $('#spot2 div:nth-child(1)').delay(200).animate({ padding: 0 }, 0, function loop() {
      $('#spot2 div:nth-child(1)').animate({ opacity: 0.1 }, 500, function() {
        $('#spot2 div:nth-child(1)').animate({ opacity: 0.8 }, 500, loop)
      });
    });

    $('#spot3 div:nth-child(1)').delay(500).animate({ padding: 0 }, 0, function loop() {
      $('#spot3 div:nth-child(1)').animate({ opacity: 0.1 }, 600, function() {
        $('#spot3 div:nth-child(1)').animate({ opacity: 0.8 }, 600, loop)
      });
    });

    $('#spot4 div:nth-child(1)').delay(700).animate({ padding: 0 }, 0, function loop() {
      $('#spot4 div:nth-child(1)').animate({ opacity: 0.1 }, 800, function() {
        $('#spot4 div:nth-child(1)').animate({ opacity: 0.8 }, 800, loop)
      });
    });



    //      ok버튼 눌렀을 때 배경 꺼지고 설명 꺼짐
    $('#section7_ex_this>div:nth-child(8)').click(function() {
      $('#section7_ex_bg').fadeOut(500)
      $('#section7_ex_this').fadeOut(500)
      $('#section7_spot>div').animate({ 'opacity': '1' }, 500)
      $('#section7_spot_ex div').fadeIn()
    })

    // section7_spot

    $('#spot1').click(function() {
      $('#section7_place').fadeIn(500)


      $('#section7_place_ex img').delay(200).animate({ 'opacity': '1', 'top': '0px' }, 800)

      $('#section7_place_spot2').delay(600).animate({ 'opacity': '1' }, 500)
      $('#section7_place_spot_ex div:nth-child(2)').delay(600).animate({ 'opacity': '1' }, 500)
      $('#section7_place_spot1').delay(750).animate({ 'opacity': '1' }, 500)
      $('#section7_place_spot_ex div:nth-child(1)').delay(750).animate({ 'opacity': '1' }, 500)
      $('#section7_place_spot3').delay(900).animate({ 'opacity': '1' }, 500)
      $('#section7_place_spot_ex div:nth-child(3)').delay(900).animate({ 'opacity': '1' }, 500)

      $('#section7_place>h5').delay(970).animate({ 'top': '250px', 'opacity': '1' }, 850)
      $('#section7_place>p').delay(1300).fadeIn()

      $('#section7_place_spot4').delay(1050).animate({ 'opacity': '1' }, 500)
      $('#section7_place_spot_ex div:nth-child(4)').delay(1050).animate({ 'opacity': '1' }, 500)

      $('#section7_spot>div').fadeOut(300)
      $('#section7_text').fadeOut(300)
      $('#section7_spot_click div').fadeOut(300)

      //            ddddddddddddddddddddddddddddddddddddddddd




    })

    $('#section7_place_spot_wrap>div:nth-child(1)').click(function() {
      $('#section7_place>h5').fadeOut(function() {
        $('#section7_place>h5').html('The fabulous Aurora Borealis is a wonderful natural phenomenon, where the sky glows with thousands of seemingly dancing lights. </br>Lets find out where are the best places to see them in Russia!').fadeIn()
      })
    })
    $('#section7_place_spot_wrap>div:nth-child(2)').click(function() {
      $('#section7_place>h5').fadeOut(function() {
        $('#section7_place>h5').html('Northern Norway is amongst the most comfortable and interesting places to see the northern lights, as hundreds of thousands</br>of people live in this huge geographical area.').fadeIn()
      })
    })
    $('#section7_place_spot_wrap>div:nth-child(3)').click(function() {
      $('#section7_place>h5').fadeOut(function() {
        $('#section7_place>h5').html('The aurora belt in Alaska’s great Interior and Arctic regions is among the most active in the world, and there are few other places</br> on earth so suited for an Aurora Borealis vacation.').fadeIn()
      })
    })
    $('#section7_place_spot_wrap>div:nth-child(4)').click(function() {
      $('#section7_place>h5').fadeOut(function() {
        $('#section7_place>h5').html('The northern lights are one of the biggest draws to visiting Iceland, however they are also one of the</br>most elusive and unpredictable attractions this country has.').fadeIn()
      })
    })


    $('#spot2').click(function() {
      $('#section7_conditions').fadeIn()

      $('#section7_conditions_right_1_m div:nth-child(1)').delay(300).animate({ 'width': '202px', 'opacity': '0.3' }, 1200)
      $('#section7_conditions_right_1_m div:nth-child(2)').delay(600).animate({ 'width': '202px', 'opacity': '0.3' }, 1200)
      $('#section7_conditions_right_1_m div:nth-child(3)').delay(900).animate({ 'width': '50px', 'opacity': '0.3' }, 1200)

      $('#section7_conditions_right_2_m div:nth-child(1)').delay(1200).animate({ 'width': '209px', 'opacity': '0.3' }, 1200)
      $('#section7_conditions_right_2_m div:nth-child(2)').delay(1500).animate({ 'width': '209px', 'opacity': '0.3' }, 1200)
      $('#section7_conditions_right_2_m div:nth-child(3)').delay(1800).animate({ 'width': '118px', 'opacity': '0.3' }, 1200)

      $('#section7_conditions_right_3_m div:nth-child(1)').delay(2100).animate({ 'width': '209px', 'opacity': '0.3' }, 1200)
      $('#section7_conditions_right_3_m div:nth-child(2)').delay(2400).animate({ 'width': '209px', 'opacity': '0.3' }, 1200)
      $('#section7_conditions_right_3_m div:nth-child(3)').delay(2700).animate({ 'width': '209px', 'opacity': '0.3' }, 1200)


      $('#section7_spot>div').fadeOut(300)
      $('#section7_text').fadeOut(300)
      $('#section7_spot_click div').fadeOut(300)
    })

    $('#spot3').click(function() {
      $('#section7_tips').fadeIn()


      $('#section7_tips_center').delay(300).fadeIn(1000)
      $('#section7_tip_line').delay(300).fadeIn(1000)
      $('#section7_tip1').delay(800).animate({ 'opacity': '1', 'top': '60px' }, 800)
      $('#section7_tip2').delay(1000).animate({ 'opacity': '1', 'top': '250px' }, 800)
      $('#section7_tip3').delay(1200).animate({ 'opacity': '1', 'top': '130px' }, 800)

      $('#section7_spot>div').fadeOut(300)
      $('#section7_text').fadeOut(300)
      $('#section7_spot_click div').fadeOut(300)
    })


    //      section7_palce_spot

    $('#section7_place_spot1 div:nth-child(3)').animate({ padding: 0 }, 0, function loop() {
      $('#section7_place_spot1 div:nth-child(3)').animate({ width: 29, height: 29, opacity: 0.1 }, 1000, function() {
        $('#section7_place_spot1 div:nth-child(3)').animate({ width: 31, height: 31, opacity: 0.4 }, 1000, loop)
      });
    });

    $('#section7_place_spot2 div:nth-child(3)').delay(300).animate({ padding: 0 }, 0, function loop() {
      $('#section7_place_spot2 div:nth-child(3)').animate({ width: 29, height: 29, opacity: 0.1 }, 1000, function() {
        $('#section7_place_spot2 div:nth-child(3)').animate({ width: 31, height: 31, opacity: 0.4 }, 1000, loop)
      });
    });

    $('#section7_place_spot3 div:nth-child(3)').delay(600).animate({ padding: 0 }, 0, function loop() {
      $('#section7_place_spot3 div:nth-child(3)').animate({ width: 29, height: 29, opacity: 0.1 }, 1000, function() {
        $('#section7_place_spot3 div:nth-child(3)').animate({ width: 31, height: 31, opacity: 0.4 }, 1000, loop)
      });
    });

    $('#section7_place_spot4 div:nth-child(3)').delay(900).animate({ padding: 0 }, 0, function loop() {
      $('#section7_place_spot4 div:nth-child(3)').animate({ width: 29, height: 29, opacity: 0.1 }, 1000, function() {
        $('#section7_place_spot4 div:nth-child(3)').animate({ width: 31, height: 31, opacity: 0.4 }, 1000, loop)
      });
    });

    $('#section7_palce_close').click(function() {
      $('#section7_place').fadeOut(500)

      //            section7_place 초기화
      $('#section7_place_ex img').css({ 'opacity': '0', 'top': '5px' })

      $('#section7_place_spot1').css({ 'opacity': '0' })
      $('#section7_place_spot_ex div:nth-child(1)').css({ 'opacity': '0' })
      $('#section7_place_spot2').css({ 'opacity': '0' })
      $('#section7_place_spot_ex div:nth-child(2)').css({ 'opacity': '0' })
      $('#section7_place_spot3').css({ 'opacity': '0' })
      $('#section7_place_spot_ex div:nth-child(3)').css({ 'opacity': '0' })

      $('#section7_place>h5').css({ 'top': '260px', 'opacity': '0' })
      $('#section7_place>p').css({ 'display': 'none' })

      $('#section7_place_spot4').css({ 'opacity': '0' })
      $('#section7_place_spot_ex div:nth-child(4)').css({ 'opacity': '0' })

      //            section7_place 초기화 끝




      $('#section7_spot>div').fadeIn(300)
      $('#section7_text').fadeIn(300)
      $('#section7_spot_click div').fadeIn(300)


    })

    $('#section7_conditions_close').click(function() {
      $('#section7_conditions').fadeOut(500)

      //             section7_conditions 초기화

      $('#section7_conditions_right_1_m div:nth-child(1)').css({ 'width': '0px', 'opacity': '0' })
      $('#section7_conditions_right_1_m div:nth-child(2)').css({ 'width': '0px', 'opacity': '0' })
      $('#section7_conditions_right_1_m div:nth-child(3)').css({ 'width': '0px', 'opacity': '0' })

      $('#section7_conditions_right_2_m div:nth-child(1)').css({ 'width': '0px', 'opacity': '0' })
      $('#section7_conditions_right_2_m div:nth-child(2)').css({ 'width': '0px', 'opacity': '0' })
      $('#section7_conditions_right_2_m div:nth-child(3)').css({ 'width': '0px', 'opacity': '0' })

      $('#section7_conditions_right_3_m div:nth-child(1)').css({ 'width': '0px', 'opacity': '0' })
      $('#section7_conditions_right_3_m div:nth-child(2)').css({ 'width': '0px', 'opacity': '0' })
      $('#section7_conditions_right_3_m div:nth-child(3)').css({ 'width': '0px', 'opacity': '0' })


      $('#section7_spot>div').fadeIn(300)
      $('#section7_text').fadeIn(300)
      $('#section7_spot_click div').fadeIn(300)
    })

    $('#section7_tips_close').click(function() {
      $('#section7_tips').fadeOut(500)

      $('#section7_tips_center').css({ 'display': 'none' })
      $('#section7_tip_line').css({ 'display': 'none' })
      $('#section7_tip1').css({ 'opacity': '0', 'top': '70px' }, 800)
      $('#section7_tip2').css({ 'opacity': '0', 'top': '260px' }, 800)
      $('#section7_tip3').css({ 'opacity': '0', 'top': '140px' }, 800)

      $('#section7_spot>div').fadeIn(300)
      $('#section7_text').fadeIn(300)
      $('#section7_spot_click div').fadeIn(300)
    })
})