$(document).ready(function() {


    $('#section1_mouse div:eq(1)').animate({ 'top': '38px', 'opacity': '0.2' }, 1000, function loopa() {
      $('#section1_mouse div:eq(1)').css({ 'top': '17px', 'opacity': '1' })
      $('#section1_mouse div:eq(1)').animate({ 'top': '38px', 'opacity': '0.2' }, 1000, loopa)
    })

    section2_work1_1_a = 1
    section2_work1_2_a = 1
    section2_work1_3_a = 1
    section2_work1_4_a = 1

    section2_work2_1_a = 1
    section2_work2_2_a = 1
    section2_work2_3_a = 1
    section2_work2_4_a = 1

    section2_work3_1_a = 1
    section2_work3_2_a = 1
    section2_work3_3_a = 1
    section2_work3_4_a = 1

    section2_work4_1_a = 1
    section2_work4_2_a = 1
    section2_work4_3_a = 1
    section2_work4_4_a = 1

    section2_work5_1_a = 1
    section2_work5_2_a = 1
    section2_work5_3_a = 1
    section2_work5_4_a = 1

    section2_work6_1_a = 1
    section2_work6_2_a = 1
    section2_work6_3_a = 1
    section2_work6_4_a = 1

    lin1 = 0;
    lin2 = 0;
    lin3 = 0;
    lin4 = 0;
    lin5 = 0;
    lin6 = 0;
    lin7 = 0;
    lin8 = 0;
    lin9 = 0;
    lin10 = 0;
    lin11 = 0;
    lin12 = 0;
    lin13 = 0;
    lin14 = 0;
    lin15 = 0;
    lin16 = 0;
    lin17 = 0;
    lin18 = 0;
    lin19 = 0;
    lin20 = 0;
    lin21 = 0;
    lin22 = 0;
    lin23 = 0;

    $(window).scroll(function() {
      sc = $(window).scrollTop();
      if (sc >= 980) {
        $('#menu').css({ 'position': 'fixed', 'top': '0px' })
      } else {
        $('#menu').css({ 'position': 'relative', 'background-color': '#fff' })
      }

      if (sc <= 300) {
        $('#menu_wrap ul li:nth-child(1) span').css({ 'color': '#5f6669' }, 500)
        $('#menu_wrap ul li:nth-child(2) span').css({ 'color': '#c6c6c6' }, 500)
        $('#menu_wrap ul li:nth-child(3) span').css({ 'color': '#c6c6c6' }, 500)
        $('#menu_wrap ul li:nth-child(4) span').css({ 'color': '#c6c6c6' }, 500)
      }

      if (sc >= 500) {
        $('#menu_wrap ul li:nth-child(2) span').css({ 'color': '#5f6669' }, 500)
        $('#menu_wrap ul li:nth-child(1) span').css({ 'color': '#c6c6c6' }, 500)
        $('#menu_wrap ul li:nth-child(3) span').css({ 'color': '#c6c6c6' }, 500)
        $('#menu_wrap ul li:nth-child(4) span').css({ 'color': '#c6c6c6' }, 500)
      }

      if (sc >= 2900) {
        $('#menu_wrap ul li:nth-child(3) span').css({ 'color': '#5f6669' }, 500)
        $('#menu_wrap ul li:nth-child(1) span').css({ 'color': '#c6c6c6' }, 500)
        $('#menu_wrap ul li:nth-child(2) span').css({ 'color': '#c6c6c6' }, 500)
        $('#menu_wrap ul li:nth-child(4) span').css({ 'color': '#c6c6c6' }, 500)

        $('#section3_ex_skill1 div:nth-child(2)').children('div').animate({ 'width': '80%' }, 1500)
        $('#section3_ex_skill2 div:nth-child(2)').children('div').animate({ 'width': '80%' }, 1500)
        $('#section3_ex_skill3 div:nth-child(2)').children('div').animate({ 'width': '90%' }, 1500)
        $('#section3_ex_skill4 div:nth-child(2)').children('div').animate({ 'width': '80%' }, 1500)
      }

      if (sc >= 3820) {
        $('#menu_wrap ul li:nth-child(4) span').css({ 'color': '#5f6669' }, 500)
        $('#menu_wrap ul li:nth-child(1) span').css({ 'color': '#c6c6c6' }, 500)
        $('#menu_wrap ul li:nth-child(2) span').css({ 'color': '#c6c6c6' }, 500)
        $('#menu_wrap ul li:nth-child(3) span').css({ 'color': '#c6c6c6' }, 500)

      }



      //1층 시작
      if (sc > 550) {
        section2_work1_1_a = section2_work1_1_a + 1

        if (section2_work1_1_a == 2) {
          $('#section2_work1 li').eq(0).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin1 == 1;
          });

        }
      }
      if (sc > 600) {
        section2_work1_2_a = section2_work1_2_a + 1

        if (section2_work1_2_a == 2) {
          $('#section2_work1 li').eq(1).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin2 == 1;
          });

        }
      }
      if (sc > 650) {
        section2_work1_3_a = section2_work1_3_a + 1

        if (section2_work1_3_a == 2) {
          $('#section2_work1 li').eq(2).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin3 = 1;
          });

        }
      }
      if (sc > 700) {
        section2_work1_4_a = section2_work1_4_a + 1

        if (section2_work1_4_a == 2) {
          $('#section2_work1 li').eq(3).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin4 = 1;
          });

        }
      }
      // 2층 시작
      if (sc > 850) {
        section2_work2_1_a = section2_work2_1_a + 1

        if (section2_work2_1_a == 2) {
          $('#section2_work2 li').eq(0).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin5 = 1;
          })

        }
      }
      if (sc > 900) {
        section2_work2_2_a = section2_work2_2_a + 1

        if (section2_work2_2_a == 2) {
          $('#section2_work2 li').eq(1).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin6 = 1;
          })

        }
      }
      if (sc > 950) {
        section2_work2_3_a = section2_work2_3_a + 1

        if (section2_work2_3_a == 2) {
          $('#section2_work2 li').eq(2).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin7 = 1;
          })

        }
      }
      if (sc > 1000) {
        section2_work2_4_a = section2_work2_4_a + 1

        if (section2_work2_4_a == 2) {
          $('#section2_work2 li').eq(3).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin8 = 1;
          })

        }
      }

      // 3층 시작
      if (sc > 1150) {
        section2_work3_1_a = section2_work3_1_a + 1

        if (section2_work3_1_a == 2) {
          $('#section2_work3 li').eq(0).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin9 = 1;
          })

        }
      }
      if (sc > 1200) {
        section2_work3_2_a = section2_work3_2_a + 1

        if (section2_work3_2_a == 2) {
          $('#section2_work3 li').eq(1).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin10 = 1;
          })

        }
      }
      if (sc > 1250) {
        section2_work3_3_a = section2_work3_3_a + 1

        if (section2_work3_3_a == 2) {
          $('#section2_work3 li').eq(2).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin11 = 1;
          })

        }
      }
      if (sc > 1300) {
        section2_work3_4_a = section2_work3_4_a + 1

        if (section2_work3_4_a == 2) {
          $('#section2_work3 li').eq(3).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin12 = 1;
          })

        }
      }
      // 4층 시작
      if (sc > 1450) {
        section2_work4_1_a = section2_work4_1_a + 1

        if (section2_work4_1_a == 2) {
          $('#section2_work4 li').eq(0).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin13 = 1;
          })

        }
      }
      if (sc > 1500) {
        section2_work4_2_a = section2_work4_2_a + 1

        if (section2_work4_2_a == 2) {
          $('#section2_work4 li').eq(1).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin14 = 1;
          })

        }
      }
      if (sc > 1550) {
        section2_work4_3_a = section2_work4_3_a + 1

        if (section2_work4_3_a == 2) {
          $('#section2_work4 li').eq(2).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin15 = 1;
          })

        }
      }
      if (sc > 1600) {
        section2_work4_4_a = section2_work4_4_a + 1

        if (section2_work4_4_a == 2) {
          $('#section2_work4 li').eq(3).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin16 = 1;
          })

        }
      }

      // 5층 시작
      if (sc > 1750) {
        section2_work5_1_a = section2_work5_1_a + 1

        if (section2_work5_1_a == 2) {
          $('#section2_work5 li').eq(0).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin17 = 1;
          })

        }
      }
      if (sc > 1800) {
        section2_work5_2_a = section2_work5_2_a + 1

        if (section2_work5_2_a == 2) {
          $('#section2_work5 li').eq(1).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin18 = 1;
          })

        }
      }
      if (sc > 1850) {
        section2_work5_3_a = section2_work5_3_a + 1

        if (section2_work5_3_a == 2) {
          $('#section2_work5 li').eq(2).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin19 = 1;
          })

        }
      }
      if (sc > 1900) {
        section2_work5_4_a = section2_work5_4_a + 1

        if (section2_work5_4_a == 2) {
          $('#section2_work5 li').eq(3).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin20 = 1;
          })

        }
      }

      // 6층 시작  
      if (sc > 2050) {
        section2_work6_1_a = section2_work6_1_a + 1

        if (section2_work6_1_a == 2) {
          $('#section2_work6 li').eq(0).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin21 = 1;
          })

        }
      }
      if (sc > 2100) {
        section2_work6_2_a = section2_work6_2_a + 1

        if (section2_work6_2_a == 2) {
          $('#section2_work6 li').eq(1).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin22 = 1;
          })

        }
      }
      if (sc > 2150) {
        section2_work6_3_a = section2_work6_3_a + 1

        if (section2_work6_3_a == 2) {
          $('#section2_work6 li').eq(2).stop().animate({ 'top': '0', 'opacity': '1' }, 500, function() {
            lin23 = 1;
          })

        }
      }
      if (sc > 2200) {
        section2_work6_4_a = section2_work6_4_a + 1
        if (section2_work6_4_a == 2) {
          $('#section2_work6 li').eq(3).stop().animate({ 'top': '0', 'opacity': '1' }, 500)
        }
      }




    });

    $('#menu_wrap ul li:nth-child(1) span').click(function() {
      $('body,html').animate({ scrollTop: 0 }, 500)
    })
    $('#menu_wrap ul li:nth-child(2) span').click(function() {
      $('body,html').animate({ scrollTop: 974 }, 500)
    })
    $('#menu_wrap ul li:nth-child(3) span').click(function() {
      $('body,html').animate({ scrollTop: 2623 }, 500)
    })
    $('#menu_wrap ul li:nth-child(4) span').click(function() {
      $('body,html').animate({ scrollTop: 4150 }, 500)
    })

  $('.guideBg div button').click(function(){
    $('.guideBg').fadeOut(500,function(){
      $('#section1_text h1').delay(600).animate({ 'opacity': '1', 'top': '0px' }, 800)
      $('#section1_text h5').delay(1000).animate({ 'opacity': '1', 'top': '70px' }, 600, function() {
        $('#section1_mouse').delay(300).fadeIn(1500)
      })
    })
  })


    $('#section2_wrap ul li').mouseenter(function() {
      if (lin1 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin2 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin3 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin4 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin5 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin6 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin7 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin8 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin9 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin10 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin11 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin12 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin13 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin14 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin15 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin16 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin17 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin18 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin19 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin20 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin21 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin22 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
      if (lin23 == 1) {
        $(this).stop().animate({ 'top': '-10px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 30px #d9d9d9' })
        $(this).children('div').stop().animate({ 'opacity': '1' }, 300)
      }
    })

    $('#section2_wrap ul li').mouseleave(function() {
      if (lin1 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin2 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin3 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin4 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin5 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin6 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin7 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin8 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin9 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin10 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin11 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin12 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin13 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin14 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin15 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin16 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin17 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin18 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin19 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin20 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin21 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin22 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }
      if (lin23 == 1) {
        $(this).stop().animate({ 'top': '0px' }, 400)
        $(this).css({ 'box-shadow': '0px 4px 10px #dedede' })
        $(this).children('div').stop().animate({ 'opacity': '0' }, 300)
      }

    })

    //    문제점 : li가 로드되기 전에 mouseenter하면 로드가 안됨
    //         : li가 차례대로 올라오지 않음

    $('#section2_next').click(function() {
      $('body,html').animate({ scrollTop: 2623 }, 500)
    })
    $('#section3_next').click(function() {
      $('body,html').animate({ scrollTop: 4150 }, 500)
    })

    $('#section4_send').click(function() {
      $('#section4_mail_pop_wrap').fadeIn(200)
      $('#section4_mail').children('div').eq(0).html('<input type="text" placeholder="NAME" id="box3" /><input type="text" placeholder="EMAIL" id="box3" />')
      $('#section4_mail').children('div').eq(1).html('<input type="text" placeholder="MESSAGE" id="box3" />')
    })
    $('#section4_mail_pop p').click(function() {
      $('#section4_mail_pop_wrap').fadeOut(200)
    })


    $('#scroll_bt').click(function() {
      $('body,html').animate({ 'scrollTop': '0' }, 700)
    })

    $(window).scroll(function() {
      sc = $(window).scrollTop();

      if (sc >= 1000) { $('#scroll_bt').fadeIn() } else if (sc <= 1000) { $('#scroll_bt').fadeOut() }
      if (sc >= 10) { $('#scroll_bt').css({ 'background': 'url("assets/images/arrow1.png") center center', 'border': '1px solid #373737' }) }
      if (sc >= 3980) { $('#scroll_bt').css({ 'background': 'url("assets/images/arrow2.png") center center', 'border': '1px solid #b8b8b8' }) }
    })



  });